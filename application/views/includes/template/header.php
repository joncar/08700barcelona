<?php if (empty($scripts)): ?>
    <div class="main-preloader">
        <div class="ball-scale">
            <div></div>
        </div>
    </div>
<?php endif ?>
<div class="into-page">
    <div class="into-cospo-gallery">
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/1.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/2.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/3.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/4.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/5.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/6.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/7.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/8.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/9.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/10.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/11.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/12.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/13.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/14.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/15.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/16.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/17.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/18.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/19.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/20.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/21.jpg"></div>
        </div>
        <div class="item">
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/22.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/23.jpg"></div>
            <div class="item-bg" data-src="<?= base_url() ?>promo/img/into-gallery/24.jpg"></div>
        </div>
    </div>
    <div class="into-page-content">
        <div class="into-page-logo logo">
            <a class="logo-link" href="<?= site_url() ?>">
                 <h1 style="font-size: 1.8rem;">
                    <?php
                        echo $this->db->get('ajustes')->row()->{'fecha_'.$_SESSION['lang']};
                    ?>
                    <br/>
                    DIGITAL SHOWROOM EDITION
                </h1>
            </a>
        </div>
        <img class="into-page-img" src="<?= base_url() ?>promo/24.svg" alt="">
        <div class="into-page-img countMain">
            <ul id="getting-started">
                <li><span class="days">00</span><p class="days_text">Dies</p></li>
                <li class="seperator">:</li>
                <li><span class="hours">00</span><p class="hours_text">Hores </p></li>
                <li class="seperator">:</li>
                <li><span class="minutes">00</span><p class="minutes_text">Minutos </p></li>
                <li class="seperator">:</li>
                <li><span class="seconds">00</span><p class="seconds_text">Seguts</p></li>
            </ul>
        </div>
        <a class="into-page-button" href="#top"></a>
        <div class="into-page-overlay"></div>
    </div>
</div>
<header class="navigation">
    <nav>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content-center">
                    <div>
                        <div class="menu_wrapper">
                            <ul class="navigation-menu test">
                                <li>
                                    <a href="#home">Home</a>
                                </li>


                                <li>
                                    <a href="#que-es">Qué ès?</a>
                                </li>
                                <?php
                                    $edicion_activa = $this->db->get_where('ediciones',array('ediciones.idioma'=>$_SESSION['lang'],'activa'=>1));
                                    if($edicion_activa->num_rows()>0):
                                ?>
                                    <li>
                                        <a href="#proxima"><?= $edicion_activa->row()->nombre ?></a>
                                    </li>
                                <?php endif ?>
                                <li>
                                    <a href="#expositors">Edicions Anteriors</a>
                                </li>
                                <li>
                                    <a href="#premsa">080 show media</a>
                                </li>

                                <li>
                                    <a href="#blog">Premsa</a>
                                </li>

                                <li>
                                    <a href="http://www.080barcelonafashion.cat/es">080</a>
                                </li>
                                <li>
                                    <a href="#faq">FAQ</a>
                                </li>
                                <li>
                                    <a href="#contacte">Contacte</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="scales"></div>
    </nav>   
    <button class="navigation-btn" data-action="go-next-page">
        <svg version="1.1" id="logo_copy_1_" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 11 14" enable-background="new 0 0 11 14" xml:space="preserve">
        <g id="logo_copy">
        <g>
        <path fill-rule="evenodd" clip-rule="evenodd" fill="#010101" d="M10.7,6.3C9.9,5.9,1.5,0.4,1.1,0.1C0.5-0.2,0,0.2,0,0.8
              c0,0.4,0,11.8,0,12.4c0,0.6,0.6,0.9,1,0.7c0.6-0.4,9.1-5.9,9.6-6.2C11.1,7.4,11.1,6.6,10.7,6.3z"/>
        </g>
        </g>
        </svg>
    </button>

    <div class="widget-lang">
        <!--<a href="#" class="navigation-link-btn" data-action="show-lang">En</a> -->
        <button class="navigation-link-btn" data-action="show-lang" style="font-size: 0.8rem;">IDIOMA</button>
        <ul>
            <li>
                <a href="<?= base_url('main/traducir/ca') ?>">Català</a>
            </li>
            <li>
                <a href="<?= base_url('main/traducir/es') ?>">Castellano</a>
            </li>
            <li>
                <a href="<?= base_url('main/traducir/en') ?>">English</a>
            </li>
        </ul>
        <div class="widget-lang-overlay"></div>
    </div>

    <div class="widget-block widget-block-bottom">
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="widget-content">

                    <div class="widget-newsletter">
                        <h5>Subscriu-te al nostre butlletí</h5>
                        <p>Rep el nostre butlletí de correu electrònic per estar informat.</p>
                        <div id="subsmessage" style="display:none;"></div>
                        <form method="post" onsubmit="return subscribir()">
                            <input type="email" name="subscribe" id="emailSub" placeholder="Sisplau deixa el teu email" autocomplete="off">
                            <button type="submit"></button>
                        </form>
                    </div>

                    <div class="widget-item center">
                        <a href="https://www.linkedin.com/company-beta/5393154/" class="button socicon-linkedin dark-blue"></a>
                        <a href="https://twitter.com/modaccat" class="button socicon-twitter light-blue"></a>
                        <a href="https://www.instagram.com/modaccat/" class="button socicon-instagram dark-red"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--<button class="navigation-link-btn" data-action="show-widget-bottom"></button>-->

    <div class="widget-overlay">
        <div class="widget-overlay-top"></div>
        <div class="widget-overlay-bottom"></div>
    </div>

</header>

<!-- Overlay -->
<div class="main-overlay hide-bg"></div>
