<script src="<?= base_url() ?>js/template/notify.js"></script>
<script src="<?= base_url() ?>js/template/jquery-ui.min.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI&language=es"></script>
<script src="<?= base_url() ?>js/template/instafeed.min.js"></script>
<script src="<?= base_url() ?>js/template/core.js"></script>
<script src="<?= base_url() ?>js/template/app.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.js"></script>
<script src="<?= base_url() ?>promo/js/classList.min.js"></script>
<script src="<?= base_url() ?>js/template/intoGallery.js"></script>
<script src="<?= base_url() ?>promo/js/main.js"></script>
    
<script>
    var config1 = {
        'id': '690475437269590016',
        'domId': 'twitter',
        'maxTweets': 1,
        'enableLinks': true,
        'showPermalinks': false,
        'showInteraction': false,
        'showTime': false
    };
    //twitterFetcher.fetch(config1);
</script>

<script src="<?= base_url() ?>js/template/jquery.countdown.js"></script>
<script>
    $("#getting-started").countdown({
      date: '2020/09/10 20:30:00',
      day: 'Dia',
      days: 'Dies',
      hour: 'Hora',
      hours: 'Hores',
      minute: 'Minut',
      minutes: 'Minuts',
      second: 'Segon',
      seconds: 'Segons'
    }, function (event) {});
</script>
<script>
    function validar(form){
        var data = new FormData(form);
        $.ajax({
            url:'<?= base_url('invitaciones/frontend/validar') ?>',
            data:data,
            type:"post",
            cache: false,
            contentType: false,
            processData:false,
            success:function(datos){
                $("#validarForm .form-message").html(datos);
            }
        });
        return false;
    }
</script>
<script>
function subscribir(){
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#emailSub").val()},function(data){
      $("#subsmessage").html(data);
      $("#subsmessage").show();
  });
  return false;
}
</script>
<script>
function contactar(){
  $.post('<?= base_url('paginas/frontend/contacto') ?>',{nom:$("#nomCon").val(),message:$("#messageCon").val(),email:$("#emailCon").val()},function(data){
      $("#Conmessage").html(data);
      $("#Conmessage").show();
  });
  return false;
}
</script>
