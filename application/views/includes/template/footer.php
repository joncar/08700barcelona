<footer class="footer padding-top120px">
    <div class="footer-widgets">
        <div class="container">
            <div class="row"> 
                <div class="col-md-5">  
                    <div class="widget widget-text">
                        <h4 class="widget-title">Què és Futur mod?</h4>
                        <div class="text">                                
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                        </div><!-- /.textwidget -->
                        <ul class="flat-socials">
                            <li class="facebook">
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="twitter">
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                   
                        </ul>
                    </div><!-- /.widget -->      
                </div><!-- /.col-md-5 --> 

                <div class="col-md-3">  
                    <div class="widget contact-info">
                        <h4 class="widget-title">Adreça</h4>
                        <ul>
                            <li class="address"><a href="#">President Lluis Companys, 28</a></li>
                            <li class="address1"><a href="#">08700 Igualada</a></li>
                            <li class="phone"><a href="#">+34 - 111 - 123456</a></li>
                            <li class="phone1"><a href="#">+34 - 111 - 123457</a></li>
                            <li class="email"><a href="#">info@futurmod.fashion</a></li>  
                        </ul>
                    </div>  
                </div><!-- /.col-md-3 -->

                <div class="col-md-4">  
                    <div class="widget widget_mc4wp">
                        <div id="mc4wp-form-1" class="form mc4wp-form clearfix">
                            <h4 class="widget-title">Subscriu-te</h4>
                            <div class="mail-chimp">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna amader liqua.</p>
                                <form action="#" id="mailform" method="get">
                                    <input type="email" id="m" class="mmm" placeholder="El teu Email">
                                    <input type="submit" value="" id="gmail">
                                </form>
                            </div>
                        </div>
                    </div>     
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->    
        </div><!-- /.container -->
    </div><!-- /.footer-widgets -->
</footer><!-- /footer -->

<div class="bottom">        
    <div class="container-bottom">
        <div class="copyright"> 
            <p>Tots els drets reservats 2017 © Hipo</p>
        </div>                

        <ul class="text-right">
            <li><a href="#">Termes i Conditions</a></li>
            <li><a href="#">Política de Pivacitat</a></li>
            <li><a href="#">Legal</a></li>
        </ul>              
    </div><!-- /.container-bottom -->            
</div><!-- /bottom -->

<!-- Go Top -->
<a class="go-top">
    <i class="fa fa-chevron-up"></i>
</a>