<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="description" content="El 080 Barcelona Fashion Showroom, un espacio en el cual las mejores marcas de estilo mediterráneo te mostrarán sus col·leccions en el marco de la passarel·la 080 Barcelona Fashion.">
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <title><?= empty($title)?'080 Barcelona Show Room':$title.'' ?></title>
    <link rel="stylesheet" href="css/template/bootstrap.css">
    <link rel="stylesheet" href="css/template/main.css">
    <link rel="stylesheet" href="<?= base_url() ?>promo/css/style.css">
    <script src="<?= base_url() ?>js/template/jquery-2.2.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
    <?php if(!empty($myscripts)){ echo $myscripts; } ?>
    <script>
    var url = '<?= base_url() ?>';
    </script>
  </head>

  <body class="dark fasion-designer">
    <?= $this->load->view('includes/template/header') ?>
    <?= $this->load->view($view); ?>
    <?php if(empty($scripts)): ?>
        <?= $this->load->view('includes/template/scripts') ?>    
    <?php endif ?>
    <?php $this->load->view('includes/aviso-cookies'); ?>
    <script>
        (function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;
            w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),
            m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://080.sherparing.com/mtc.js','mt');

        mt('send', 'pageview');
    </script>
    <script src="https://080.sherparing.com/focus/1.js" type="text/javascript" charset="utf-8" async="async"></script>
  </body>
  

</html>
