﻿<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    public $theme = '';
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        $this->load->library('traduccion');
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'ca_ES', 'Catalan_Spain', 'Catalan');
        if(empty($_SESSION['lang']) || empty($_SESSION['langNotif'])){
            $_SESSION['lang'] = 'es';
            $_SESSION['langNotif'] = '5';
        }
        switch($_SESSION['lang']){
            case 'es': $this->config->set_item('language', 'spanish'); break;
            case 'en': $this->config->set_item('language', 'english'); break;
            case 'ca': $this->config->set_item('language', 'catalan'); break;
        }
    }
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        //$blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');     
        $blog->where('status',1);
        $blog->where('idioma',$_SESSION['lang']);
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }      
    
    function set_invitacion($accion = '',$return = false)
    {
         
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
        
        $invitacion = new ajax_grocery_CRUD();
        $invitacion->set_theme('crud');
        $invitacion->set_table('invitaciones');
        $invitacion->set_subject('invitaciones');
        $invitacion->set_rules('invitados','Invitados','required|integer|less_than[3]');
        $invitacion->set_rules('email','Email','required|valid_email|is_unique[invitaciones.email]');
        $invitacion->display_as('nombre','Nom')
                   ->display_as('apellido_1','Cognoms 1')
                   ->display_as('apellido_2','Cognoms 2')
                   ->display_as('email','Email')
                   ->display_as('dni','DNI')
                   ->display_as('telefono','Telèfon')
                   ->display_as('direccion','Adreça')
                   ->display_as('cp','CP')
                   ->display_as('poblacion','Població')
                   ->display_as('pais','País')
                   ->display_as('empreas','Empresa')
                   ->display_as('carrec','Càrrec')
                   ->display_as('cif','CIF');
        $invitacion->required_fields_array();
        $invitacion->set_lang_string('form_save','Enviar');
        $invitacion->callback_before_insert(function($post){
                get_instance()->load->model('querys');
                $post['nro'] = get_instance()->querys->get_key();                
                $this->enviarcorreo((object)$post,1,'info@080barcelonashowroom.com');                
                return $post;
        });
        $invitacion->set_placeholder();
        $invitacion->set_url('main/set_invitacion/');
        $invitacion->field_type('status','invisible')
                   ->field_type('nro','invisible')
                    ->field_type('titulo','invisible')
                    ->field_type('mensaje','invisible')
                   ->field_type('tipologia_comprador','enum',array('Alga Multimarca','Agente','Distribuidor','Otros'))
                   ->field_type('idioma','hidden',$_SESSION['lang'])
                   ->field_type('fecha_aceptacion','invisible');
                         
        $invitacion->unset_back_to_list();
        $invitacion->unset_jquery();
        $accion = !is_numeric($accion)?'':$accion;
        $invitacion = $invitacion->render($accion,'application/modules/paginas/views/');
        if($this->db->get('ajustes')->row()->permitir_subscripciones==0){
            switch($_SESSION['lang']){ 
                case 'es': $invitacion->output = '<h3>El periodo de solicitud de acreditaciones está cerrado. Perdonad las molestias.</h3>'; break;
                case 'ca': $invitacion->output = '<h3>El periode de solicitut d\'acreditacions s’ha tancat. Perdoneu les molesties.</h3>'; break;
                case 'en': $invitacion->output = '<h3>The application period for accreditation is closed. Sorry for the inconvenience.</h3>'; break;
            }
        }
        
        if($return){
            return $invitacion;
        }
        
        $this->loadView($invitacion);
    }

    public function index() {
        $this->get_entries();                
        $cruds = $this->set_invitacion(2,true);
        $header = get_header_crud($cruds->css_files, $cruds->js_files,TRUE);
        $this->loadView(array('view'=>'main','blog'=>$this->blog,'solicitar'=>$cruds->output,'myscripts'=>$header));
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if(is_string($param)){
            $param = array('view'=>$param);
        }
	$view = $this->load->view('template', $param,TRUE);
        $ajustes = $this->db->get_where('ajustes')->row();
        if(!empty($ajustes->analytics)){
            $view= str_replace('</body>',$ajustes->analytics.'</body>',$view);
        }
        echo $this->traduccion->traducir($view,$_SESSION['lang']);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }
            $mensaje->texto = str_replace('//','/',$mensaje->texto);
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            //correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
            //correo('info@hipo.tv',$mensaje->titulo,$mensaje->texto);
        }
        
    public function traducir($idioma = 'ca'){        
        $_SESSION['lang'] = $idioma;
        switch($idioma){
            case 'ca': $_SESSION['langNotif'] = '2'; break;
            case 'es': $_SESSION['langNotif'] = '5'; break;
            case 'en': $_SESSION['langNotif'] = '6'; break;
        }        
        if(empty($_GET['url'])){
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            redirect($_GET['url']);
        }
    }

}
