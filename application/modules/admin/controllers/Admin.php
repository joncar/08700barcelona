<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{        
        function __construct() {
            parent::__construct();                     
        }        
        
        function ajustes(){
            $crud = $this->crud_function("","");
            $crud->unset_add()->unset_delete()->unset_read()->unset_print()->unset_export(); 
            $crud->field_type('permitir_subscripciones','checkbox');
            $crud = $crud->render();
            $this->loadView($crud);
        }
       
    }