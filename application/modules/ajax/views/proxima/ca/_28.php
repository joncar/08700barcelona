<div class="ajax-gallery-title">
  <h2>Marques</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">DESIGNERS SOCIETY
    <span>Womenwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/designers.jpg" alt="">
    <h4>Barcelona</h4>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Des de 2017</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          
      </svg>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    La nostra roba és reflex de la bellesa l'amor i la col·laboració entre els dissenyadors de tot el món. Creiem que quan porta a diferents ments creatives, es generaria una cosa inspiradora.  <p>
  Som un equip jove i creatiu amb una gran quantitat d'experiència en la indústria de la moda. Ens encanta col·laborar amb artistes de diferents disciplines com a dissenyadors gràfics, dissenyadors de productes, blocaires o fotògrafs. És important per a nosaltres ser part d'un equip multidisciplinari en què diferents punts de vista estimulants convertit.  <p>
  <p>
  Dirigit per Pep Va generar (CEO de Yerse durant 18 anys), treballem junts per construir un nou i inspirador marca de roba.
    <figure>
    <img src="<?= base_url() ?>img/services/designers1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/designers2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/designers3.jpg" alt=""/>
  </figure>
  

</div>