<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">MESSCALINO
    <span>Womenwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/messcalino.jpg" alt="">
    <h4>Igualada</h4>
    <span>Itàlia, 10 - Tel. +34 938 04 53 06</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Desde 1988</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://messcalino.es">www.messcalino.es</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Agnès Ros se inició en el mundo del diseño en su localidad natal, Ponts (Lleida). Apasionada de la moda, empezó pronto a viajar a París, Milán, Nueva York, Londres,…Posteriormente, se trasladó a Igualada, población de gran tradición textil, donde se incorporó como diseñadora colaborando con varias firmas de género de punto. En 1988, junto a su marido, Sebastià Lloret, creó la empresa MESSCALINO.
  </p>
  <p>
    “Procuro que mis colecciones estén muy equilibradas. Para mí es primordial la fase de inicio de la colección, con la selección de las materias primas, ya sean hilos para el punto o estampados y tejidos, que prefiero sean nobles y naturales… A continuación, la parte más creativa, donde definimos las formas y los acabados de cada modelo y donde puedo plasmar mi estilo”.  </p>
  <p>
    MESSCALINO és una firma de moda para una mujer dinámica, activa y moderna, que gusta de la ropa de calidad, cómoda y con un diseño original.
    </p>
  <p>
    Las colecciones de MESSCALINO se caracterizan por un cuidado diseño, que combina texturas, estampados y acabados, para dar origen a prendas elegantes y que transmiten personalidad.  
	</p>
	<p>
    Con dos colecciones al año, MESSCALINO está presente en el mercado español a través de una red de ocho tiendas propias, corners en más de 50 centros “El Corte Inglés” y más de 750 tiendas multimarca.
	</p>
	<p>
    MESSCALINO exporta sus colecciones a Holanda, Irlanda, Japón, Portugal, Alemania, Italia y Francia y tiene previsto ampliar su presencia internacional a USA y Reino Unido.
	</p>

  <figure>
    <img src="<?= base_url() ?>img/services/messcalino1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/messcalino2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/messcalino3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/messcalino5.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/messcalino4.jpg" alt=""/>
  </figure>

</div>