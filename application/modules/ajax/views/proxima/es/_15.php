<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">WOM&NOW
    <span>Womenswear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/wn.jpg" alt="">
    <h4>Vacarisses</h4>
    <span>Berlín, 2 Bis - Tel. +34 93 828 18 10</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Desde 1960</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.womandnow.com">www.womandnow.com</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    W O M & N O W es una empresa de moda dedicada al diseño y la confección de prendas urbanas en clave de luxe; una fórmula que seduce a millones de mujeres. Prendas de corte perfecto que imprimen cierto allure, y que a su vez son cómodas de llevar.
  </p>
  <p>
    Con más de 50 años de experiencia profesional, impregnamos nuestra magia y creatividad en cada una de nuestras prendas consiguiendo así que cada mujer se sienta única y especial en cada momento del día.
  <p>
  <p>
    El exquisito mundo de la confección y el particularísimo hacer de nuestros maestros de la aguja son el culmen de nuestras colecciones y un referente a seguir. Un servicio acorde con los tiempos, en el que la moda nos descubre una nueva manera de entender el gusto y el placer de vestir bien. Piezas inéditas a partir de las telas más exclusivas, líneas puras y casi arquitectónicas, cortes exquisitos, siluetas perfectamente delineadas y originalidad en los detalles.
  <p>
  <p>
    Todos ellos ingredientes de una elegancia de vanguardia y de una sastrería única. Una tradición que se renueva cada temporada y colección, siempre bajo los parámetros y las técnicas del sartorial más refinado, riguroso, funcional y siempre innovador. Cuidamos cada detalle en el diseño, patronaje y confección, utilizando tejidos, forros y fornituras de calidad, para que se sientan cómodas con prendas de plana, tricot y circular.
  <p>
  <p>
    Nuestras colecciones están diseñadas para mujeres dinámicas, cosmopolitas y exigentes que reinterpretan sus outfits cotidianos a través de prendas y accesorios de silueta impecable, altas dosis de feminidad y con un twist contemporáneo, exentos de ornamentos ni detalles superfluos.
  <p>

  <figure>
    <img src="<?= base_url() ?>img/services/wn1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/wn2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/wn3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/wn4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/wn5.jpg" alt=""/>
  </figure>

</div>