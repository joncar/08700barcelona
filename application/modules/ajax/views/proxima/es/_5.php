<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">ESTER FERRANDO
    <span>Women Knitwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/ef.jpg" alt="">
    <h4>Vilanova i la Geltrú</h4>
    <span>C/ Llibertat, 67 Baixos - Tel. +34 938 152 358</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Desde 2012</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.efk-bcn.com">www.efk-bcn.com</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Ester Ferrando nació en Vilanova i la Geltrú, Barcelona. Desde muy joven comenzó a desarrollar su pasión por el tejido a mano y el ganchillo. Después de terminar su carrera en la Escola d’Arts i Tècniques de Barcelona (ESDI), especializada en géneros de punto, se graduó con honores en moda de la prestigiosa Winchester School of Arts (Reino Unido).
  </p>
  <p>
    Apasionada, curiosa e innovadora, Ester comenzó a trabajar como asistente de diseño en varios proyectos en los estudios Pineapple en Londres y Studio Orta en París.
  </p>
  <p>
    De vuelta en Barcelona, ​​colaboró ​​con prestigiosas marcas y diseñadores como Josep Font, Javier Simorra, Pedro Morago o Georgina Vendrell. A lo largo de los años siguientes estuvo comprometida con la educación, enseñando Diseño Técnico y Prendas de Vestir en diversas Escuelas de Moda de Barcelona.
  </p>
  <p>
    En el año 2012 Ester lanza su propia empresa, ESTER FERRANDO - KNITWEAR, desde su taller en Barcelona, ​​totalmente enfocada a los géneros de punto. La exquisita calidad de los materiales ha sido, desde el principio, una de las principales características de sus creaciones. Con ellas crea y desarrolla prendas con texturas originales, efectos visuales y exquisitas combinaciones coloridas.  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/ef1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/ef2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/ef3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/ef4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/ef5.jpg" alt=""/>
  </figure>

</div>