<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">LOLA CASADEMUNT
    <span>Womenswear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/lola.jpg" alt="">
    <h4>Cardedeu</h4>
    <span>C-251, 5 - Tel. +34 93 879 39 38</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Desde 1981</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.lolacasademunt.com">www.lolacasademunt.com</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Variedad, estilo, imagen y moda son conceptos íntimamente ligados y siempre presentes en Lola Casademunt. Desde nuestros inicios en 1981, el diseño, la fabricación y la distribución de nuestras propias colecciones han sido nuestra mejor garantía de consolidación en el mercado.<br><br>
    Lola Casademunt empezó su actividad en  Cardedeu (provincia Barcelona) de la manera más casual, en su domicilio particular empezó a realizar artesanalmente complementos de pelo tales como lazos, diademas y accesorios especialmente de terciopelo con un estilo propio que aún hoy se sigue manteniendo. Sus trabajos fueron descubiertos por una comercial del sector y a partir de aquí empezó la comercialización de sus diseños. Lola Casademunt viendo el futuro que podía tener, no desaprovechó la ocasión y en poco tiempo distribuía su producto en los comercios más cercanos, y en menos de dos años ya se podían adquirir productos Lola Casademunt en toda España.
No sólo crecieron los puntos de venta, sinó que también se amplió la colección diseñando bisutería y complementos de moda para la mujer.
Nuestros productos están presentes en reconocidos establecimientos de prestigio como El Corte Inglés, dedicados en exclusiva a la belleza y la moda. Temporada tras temporada todas nuestras colecciones siguen el mismo criterio: ofrecer un gran surtido acorde con las últimas tendencias, manteniendo al mismo tiempo el carácter e identidad propios y únicos de Lola Casademunt.<br><br>
  A lo largo de los años la dirección de la empresa ha recaído en Maite y Mª Mar Gassó, hijas de Lola Casademunt uniendo experiencia, ilusión y el empuje de la juventud.<br><br>
  Con el fin de abrir nuevos horizontes hacia el mercado internacional, y para ofrecer un mejor servicio, Lola Casademunt ha trasladado su actividad a unas nuevas y modernas instalaciones de 3.000 m2. Un nuevo espacio donde seguimos proporcionando los máximos estándares de calidad e innovación, de la misma forma que hemos hecho ya durante nuestros 29 años de experiencia.
  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/lola1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/lola2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/lola3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/lola4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/lola5.jpg" alt=""/>
  </figure>

</div>