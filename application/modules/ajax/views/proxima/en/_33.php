<div class="ajax-gallery-title">
  <h2>Frands</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">DIVAS
    <span>Womenwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/divas.jpg" alt="">
    <h4>Sabadell</h4>
    <span>C/ Raimon Casellas, 26 - +34 93 711 69 11 </span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Since 1979</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.divas.es">www.divas.es</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    DIVAS is a Spanish brand distributed by DIVAS CONFEC S. L. specialized in the design, production and commercializing of prêt-a-porter women's clothing.
  <p>
  <p>
  "Divas allows urban women to dress elegant and different at affordable price". We love that our clothes let you express your personality and therefore we are taking care of every detail.
  <p>
  <p>
  The brand was established in the year 1979 as the lead of a textile company founded in 1966.
  <p>
  <p>
  Our main goal is to satisfy the demands of the most current trends in fashion by applying an elegant and urban style but never giving up the comfort.
  <p>
  <p>
  The experience of all these years helps us to evolve continually and be able to meet and understand the needs of our customers in terms of design, quality and fitting.
  <p>
  <p>
  Our factory is located in Sabadell (Barcelona), as the majority of our products are produced in Spain we are able to react and response quickly to the changes and demands of the market , to garanty that our articles are made in decent working conditions and by avoiding transports we are actively contributing in protecting the environment and the climate.
  <p>
  <p>
  Until 1997 our principal segment of market was the retail in Spain, and then little by little Divas was established in countries like Portugal, Belgium, Germany, England, Ireland, Finland, Sweden, the Netherlands, France and Italy.
  <figure>
    <img src="<?= base_url() ?>img/services/divas1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/divas2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/divas3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/divas4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/divas5.jpg" alt=""/>
  </figure>

</div>