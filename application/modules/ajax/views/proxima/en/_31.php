<div class="ajax-gallery-title">
  <h2>Brands</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">HELS BCN
    <span>Swimwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/hels.jpg" alt="">
    <h4>Barcelona</h4>
    <span>Passeig de Picasso, 36 </span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Since 2012</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.helsbcn.com">www.helsbcn.com</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Hels BCN is a swimwear brand for those who think that summer should last forever; spending days with sandy feet and salty hair, diving into the ocean, the smell of coconut oil, the taste of an ice-cream, and the joy of being surrounded by your beloved ones. That's why we believe the best memories are those made under the sun.
  <p>
  <p>
  Loving the ocean, the sky, the sand and the sun inspires us. Every piece of swimwear is handcrafted in Barcelona with love. We create a product that suits our adventurous spirit & lifestyle, non other than making new discoveries and enjoying what you do. There is love, history, passion and a breath of fresh air in every piece we create. Our designs are the mixture of Mediterranean essence and Brazilian cuts.
  <p>
  <p>
  Every Hels BCN suit is one of a kind in different combination of colors, prints, fabrics, and styles.
It's perfect fit and seamless comfort give our pieces a unique feeling.
  <figure>
    <img src="<?= base_url() ?>img/services/hels1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/hels2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/hels3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/hels4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/hels5.jpg" alt=""/>
  </figure>

</div>