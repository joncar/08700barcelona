<div class="ajax-gallery-title">
  <h2>Brands</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">WOM&NOW
    <span>Womenswear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/wn.jpg" alt="">
    <h4>Vacarisses</h4>
    <span>Berlín, 2 Bis - Tel. +34 93 828 18 10</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Since 1960</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.womandnow.com">www.womandnow.com</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    W O M & N O W is a fashion company dedicated to the design and tailoring of deluxe urban clothing; a formula that seduces millions of women. Perfectly tailored articles with that alluring touch, which in turn are also comfortable to wear.
  </p>
  <p>
    Enjoying more than fifty years of professional experience, we permeate our magic and creativity into each and every one of our garments, hence assuring that each woman feels unique and special at all times.
  <p>
  <p>
    The exquisite world of tailoring and the very particular know-how of our needle masters represent the pinnacle of our collections and a referent to be followed. Service that is a standard of the pace of our times, in which fashion reveals a new way of understanding the delight and pleasure of dressing well. Distinct articles crafted with the most exclusive of fabrics, pure, almost architectonic lines, exquisite cuts, perfectly formulated silhouettes and originality in the details.
  <p>
  <p>
    All features of cutting edge elegance and unique tailoring. A tradition that is upheld and revamped in each season and collection, always under the parameters and techniques of the most refined, rigorous, functional and invariable innovative sartorial craft. We put painstaking care into each and every detail of the design, pattern and tailoring, using quality fabrics, linings and fasteners, so that everyone can feel comfortable in clothing crafted with flat fabric, knitted fabric and circular stitch techniques.
  <p>
  <p>
    Our collections are designed for dynamic, cosmopolitan women that are exacting in their tastes, for women who reinterpret their everyday outfits by way of garments and accessories with an impeccable silhouette, tremendous measures of femininity and with a contemporary twist, unhampered by ornaments and superfluous details.
  <p>

  <figure>
    <img src="<?= base_url() ?>img/services/wn1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/wn2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/wn3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/wn4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/wn5.jpg" alt=""/>
  </figure>

</div>
 -->