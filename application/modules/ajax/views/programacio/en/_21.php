<div class="ajax-gallery-title">
  <h2>Brands</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">KAUILA BARCELONA
    <span>Moda made & design in Barcelona</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/kauila.jpg" alt="">
    <h4>Sitges</h4>
    <span>C/Serra del Lladre, 25 - Tel. 687 89 63 39</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.kauila.es">www.kauila.es</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Kauila es una firma de ropa de Barcelona fundada por la directora creativa Anna Cortès. Sobre la base de la diferenciación y comodidad, su producción consiste completamente en tejidos de calidad, combinados con detalles únicos, lo que hace que muchas de las prendas sean auténticas "ediciones limitadas".
  <br><br> Los acabados de algunas de nuestras prendas, como nuestra chaqueta Malhia, son confeccionadas por una empresa con sede en Paris, que teje para la alta costura con telares manuales, conservando los métodos más tradicionales.

La creatividad y la originalidad de estos tejidos se complementan con telas de alta calidad.<br><br>
En Kauila queremos ofrecer la ropa que va a durar mucho más tiempo que sólo una temporada, es por eso que creemos en diseño, naturalidad, feminidad, confort, calidad, saber hacer, y sobre todo honestidad.</p>

  <figure>
    <img src="<?= base_url() ?>img/services/kauila1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila5.jpg" alt=""/>
  </figure>

</div>
 -->