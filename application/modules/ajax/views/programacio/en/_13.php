<div class="ajax-gallery-title">
  <h2>Brands</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">RITA ROW
    <span>Womenswear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/ritarow.jpg" alt="">
    <h4>Figueres</h4>
    <span>C/ Fossos, 6 - Tel. +34 665 395 802</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Since 2013</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.ritarow.com">www.ritarow.com</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Rita Row neix a Figueres (Girona) al juny de 2013, i la formen dues meitats, Imma Serra i Xènia Semis.
  </p>
  <p>
    Imma Serra. Graduada en disseny de moda en BAU Escola de Disseny a Barcelona. Va estudiar Joieria amb Susana Alonso durant 3 anys i va cursar CoolHunting en St. Martins School a Regne Unit. Va treballar per a Laura Figueras en la seva etapa Bambi by Laura. 
  <p>
    Xènia Semis. Graduada en Disseny de moda i Postgrau en Estilisme i Visual Marxandatge per IDEP Abat Oliva Barcelona. Abans de començar amb la marca va passar per Gestmusic, Martin Lamothe i Texangie.
    </p>
    <p>
    RITA ROW ofereix una nova generació de peces de qualitat a dones modernes que busquen peces distintives. Peces oversize, còmodes, femenines i pensades per ser portades alldaylong.
    </p>
    <p>
    RITA ROW és el personatge imaginari que serveix a les seves dissenyadores com a font d'inspiració per desenvolupar les seves col·leccions, des de la Costa Brava, per a una dona actual, treballadora, i lliure. Cada col·lecció s'inspira en un tema, creant peces delicades i senzilles, amb una elegància contemporània, urbana i còmoda. L'univers de Rita s'ha definit en alguna ocasió com a “còmode i bonic, oversize i sexy, que respira aquest aire mediterrani que solament poden tenir les coses que es dissenyen prop del mar”.
    </p>
    <p>
    La producció es realitza en tallers de Mataró, Igualada i Girona, promovent el compromís amb la indústria de proximitat i qualitat. 
    </p>
    <p>
    Rita Row és un projecte amb ànima integral, que combina tradició, consciència, disseny i exclusivitat.
    </p>
    <p>
    En 2014 RITA ROW va donar el gran salt en la seva primera aparició en la passarel·la 080 Barcelona Fashion amb la seva col·lecció WOW AW1516. 2017 comença com un any decisiu, els nous punts de venda en 7 nous països han donat el tret a la marca per veure i comprovar que la seva roba no té fronteres i hi ha noies “RITA” a tot el món disposades a conèixer i provar les seves peces.
    </p>

  <figure>
    <img src="<?= base_url() ?>img/services/rr1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/rr2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/rr3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/rr4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/rr5.jpg" alt=""/>
  </figure>

</div>
 -->