<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">RITA ROW
    <span>Womenswear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/ritarow.jpg" alt="">
    <h4>Figueres</h4>
    <span>C/ Fossos, 6 - Tel. +34 665 395 802</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Desde 2013</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.ritarow.com">www.ritarow.com</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Rita Row nace en Figueres (Girona) en Junio de 2013, y la forman dos mitades, Imma Serra y Xènia Semis.
  </p>
  <p>
    Imma Serra. Graduada en diseño de moda en BAU Escola de Disseny en Barcelona. Estudió Joyería con Susana Alonso durante 3 años y cursó CoolHunting en St. Martins  School en Reino Unido.  Trabajó para Laura Figueras en su etapa Bambi by Laura.
  <p>
    Xènia Semis. Graduada en Diseño de moda y Postgrado en Estilismo y Visual Merchandising por IDEP Abat Oliva Barcelona. Antes de empezar con la marca pasó por Gestmusic, Martin Lamothe y Texangie.
    </p>
    <p>
    RITA ROW ofrece una nueva generación de prendas de calidad a mujeres modernas que buscan prendas distintivas. Piezas oversize, cómodas, femeninas y pensadas para ser llevadas alldaylong.
    </p>
    <p>
    RITA ROW es el personaje imaginario que sirve a sus diseñadoras como fuente de inspiración para desarrollar sus colecciones, desde la Costa Brava, para una mujer actual, trabajadora, y libre. Cada colección se inspira en un tema, creando prendas delicadas y sencillas, con una elegancia contemporánea, urbana y cómoda. El universo de Rita se ha definido en alguna ocasión como  “cómodo y bonito, oversize y sexy, que respira ese aire mediterráneo que solo pueden tener las cosas que se diseñan cerca del mar”.
    </p>
    <p>
    La producción se realiza en talleres de Mataró, Igualada y Girona, promoviendo el compromiso con la industria de proximidad y calidad. 
    </p>
    <p>
    Rita Row es un proyecto con alma integral, que combina tradición, consciencia, diseño y  exclusividad.
    </p>
    <p>
    En 2014 RITA ROW dio el gran salto en su primera aparición en la pasarela 080 Barcelona Fashion con su colección WOW AW1516. 2017 empieza como un año decisivo, los nuevos puntos de venta en 7 nuevos países han dado el pistoletazo a la marca para ver y comprobar que su ropa no tiene fronteras y hay chicas “RITA” en todo el mundo dispuestas a conocer y probar sus prendas.
    </p>

  <figure>
    <img src="<?= base_url() ?>img/services/rr1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/rr2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/rr3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/rr4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/rr5.jpg" alt=""/>
  </figure>

</div>
 -->