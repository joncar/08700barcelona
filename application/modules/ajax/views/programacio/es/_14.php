<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">SWEET MATITOS
    <span>Womenswear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/sm.jpg" alt="">
    <h4>El Masnou</h4>
    <span>C/Fontanills, 13 - Tel. 902 43 13 12</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Desde 2017</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.sweetmatitos.com">www.sweetmatitos.com</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    BIOGRAFÍA <br>
    Sweet Matitos nace del sueño y del lifestyle de una pareja, Matías Jaramillo y Tito Maristany. Dos jóvenes pero experimentados empresarios dedicados a diferentes sectores de la moda cuya pasión por ésta, por la elegancia y por la belleza les lleva a crear su propia marca de mujer. 
  </p>
  <p>
    Su conocimiento del mundo de la moda y el amor de Matías y Tito (familiarmente llamados Matitos) por la vida, la familia, la naturaleza y el trabajo bien hecho se refleja en cada detalle del nacimiento de la colección y “la mujer” Sweet Matitos.
  <p>
  <p>
    Sweet Matitos representa a una mujer con clase, auténtica, femenina, sensible, libre y amante del lujo en su significado más amplio.
  <p>
  <p>
    COLECCIÓN- SWEET LIFE<br>

La colección de primavera verano 2018, elegante y alegre se divide en cuatro familias Romantic Beauty, Serenity, Nature Lover y Heritage.
Todas ellas representadas a través de vestidos, pantalones, chaquetas, camisas, tops, faldas y accesorios.  <p>
  <p>
    1.- ROMANTIC BEAUTY: Esta família viaja a través del romanticismo y su expresión a través de las diferentes culturas. Encontramos desde  tejidos fluidos, como la bambula y la seda en estampados o bordados de flores en tono suaves de rosas, rojos y amarillos a un romanticismo más étnico con tejidos brillantes con cuerpoy colores fuertes.
  <p>
  <p>
    2.- SERENITY: Esta segunda familia se inspira en la paz que nos aportan el contacto con la tierra. Entre ellos se inspira en el mar a través de crepes, tules bordados y otrostejidos fluidos en tonalidades azules, acompañados conestampados y bordados de corales; y en la tierra con sustonos granates, beiges y flores. 
  <p>
  <p>
    3.- NATURE LOVER: En esta línea se plasma la unión de la colección con la naturaleza. 
La bambula y el crepe en vestidos, tops y shorts abrazan  los verdes en sus diferentes tonalidades, en particular el caqui y la mezcla en estampados jungla  en tonos muydulces. La pedrería da un toque de lujo.
  <p>
  <p>
    4.- HERITAGE: La familia y el linaje caracterizan esta cuarta línea. 
Tejidos con cuerpo como tules bordados y otro másfluidos como crepes en blanco, negro y Dorado combinana la perfección para dar elegancia, estilo, arraigo y majestuosidad a las formas a través de vestidos, tops y pantalones principalmente.
  <p>

  <figure>
    <img src="<?= base_url() ?>img/services/sm1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/sm2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/sm3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/sm4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/sm5.jpg" alt=""/>
  </figure>

</div>
 -->