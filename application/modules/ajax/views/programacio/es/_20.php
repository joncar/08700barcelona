<div class="ajax-gallery-title">
  <h2>Marcas</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">MISS ETERN
    <span>Womenwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/etern.jpg" alt="">
    <h4>Barcelona</h4>
    <span>C/ Mestre Nicolau, 23 - Tel. +34 936 316 592</span>
    

    
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.missetern.com">www.missetern.com</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Somos un grupo de profesionales con más de 25 años de experiencia en el mundo de la alta moda vistiendo a mujeres de todo el mundo tanto para las ocasiones más especiales como para el día a día.
  </p>
  <p>
Diseñadores, estilistas, patronistas, cortadores, costureras y gestores colaborando juntos para conseguir un mismo objetivo: conseguir que nuestra clienta se sienta única.
  </p>
  <p>
   Durante todo el año vestimos a la mujer de una manera sobria y elegante para que se pueda diferenciar y sentirse eterna, a través de la máxima calidad.
  <p>
    Cada prenda es la culminación de un laborioso trabajo artesanal que empieza en el diseño y acaba en la confección de la propia prenda con una costura y patrón excepcionales, respetando el medio ambiente y la responsabilidad social.
  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/etern1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/etern2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/etern3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/etern5.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/etern4.jpg" alt=""/>
  </figure>

</div>
 -->