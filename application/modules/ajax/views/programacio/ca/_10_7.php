<div class="ajax-gallery-title">
  <h2>Pasarel.les</h2>
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Creating proficient marketing strategies to drive a continuous stream of profitable traffic to your website.
  </p>
  <p>
    We will not only help you get your business online, but also help to drive a continuous stream of profitable traffic to your Website through the number of programs. We create proficient marketing strategies to get you more users. We can also help you
    integrate your offline promotions with your online activities.
  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/services-element-10.jpg" alt=""/>
  </figure>

  <p>
    Creating proficient marketing strategies to drive a continuous stream of profitable traffic to your website.
  </p>
  <p>
    We will not only help you get your business online, but also help to drive a continuous stream of profitable traffic to your Website through the number of programs. We create proficient marketing strategies to get you more users. We can also help you
    integrate your offline promotions with your online activities.
  </p>

</div>