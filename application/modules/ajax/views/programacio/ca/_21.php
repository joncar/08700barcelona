<div class="ajax-gallery-title">
  <h2>Marques</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">KAUILA BARCELONA
    <span>Moda made & design in Barcelona</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/kauila.jpg" alt="">
    <h4>Sitges</h4>
    <span>C/Serra del Lladre, 25 - Tel. 687 89 63 39</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.kauila.es">www.kauila.es</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Kauila és una firma de roba de Barcelona fundada per la directora creativa Anna Cortès. Sobre la base de la diferenciació i comodidat, la seva producció consisteix completament en teixits de qualitat, combinats amb detalls únics, cosa que fa que moltes prendes siguin autèntiques "edicions limitades".
  <br><br> Els acabats d'algunes de les  nostres prendes, com la nostra jaqueta Malhia, són confeccionades per una empresa amb seu a París, que teixeix per a l'alta costura amb telers manuals, conservant els mètodes més tradicionals.

La creativitat i l'originalitat d'aquests teixits es complementen amb telers d'alta qualitat.<br><br>
A Kauila volem oferir la roba que durarà molt més que una temporada, per aquest motiu creiem en el disseny, la naturalitat, la feminitat, el confort, la qualitat, el saber fer y sobretot la honestedat.</p>

  <figure>
    <img src="<?= base_url() ?>img/services/kauila1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/kauila5.jpg" alt=""/>
  </figure>

</div>