<div class="ajax-gallery-title">
  <h2>Marques</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">MISS ETERN
    <span>Womenwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/etern.jpg" alt="">
    <h4>Barcelona</h4>
    <span>C/ Mestre Nicolau, 23 - Tel. +34 936 316 592</span>
    

    
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.missetern.com">www.missetern.com</a></span>
    </div>
  </div>

  

<!-- 
</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Som un grup de professionals amb més de 25 anys d’esperiència en el mon de l’alta costura vestint a dones de tot el món, tant per ocasions especials com pel dia a dia. 
  </p>
  <p>
Disenyadors, estilistes, patronistes, talladors, costureres i gestors col·laborant junts per aconseguir un mateix objectiu: que la nostra clienta se senti única.
  </p>
  <p>
   Durant tot l'any vestim a la dona d'una manera sòbria i elegant perquè es pugui diferenciar i sentir-se eterna, a través de la màxima qualitat.
  <p>
    Cada peça és la culminació d'un laboriós treball artesanal que comença en el disseny i acaba en la confecció de la pròpia peça amb una costura i patró excepcionals, respectant el medi ambient i la responsabilitat social.
  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/etern1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/etern2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/etern3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/etern5.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/etern4.jpg" alt=""/>
  </figure>

</div>
 -->