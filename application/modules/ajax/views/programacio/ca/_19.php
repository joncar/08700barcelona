<div class="ajax-gallery-title">
  <h2>Marques</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">NURIA FERRER
    <span>Swimwear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/nferrer.jpg" alt="">
    <h4>Campllong</h4>
    <span>C/ Camí de Riudellots, 8 - Tel. +34 972 463 141</span>
    

    
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://www.nuriaferrer.com">www.nuriaferrer.com</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Núria Ferrer és una marca espanyola especialitzada en el disseny i fabricació de vestit de bany per a la dona. La fabricació està feta a les nostres instal·lacions i cada peça està elaborada de manera artesanal. Els dissenys i estampats son exclusius de la nostra marca Núria Ferrer®. </p>
  <p>
L'equip de disseny, dirigit per Núria Ferrer, està permanent en contacte amb les noves tendències de la moda i selecciona els millors teixits procedents de Barcelona i Milà. Cada peça està feta a la nostra empresa amb un elegant disseny per oferir una col·lecció única i especial.
  </p>
  <p>
Núria Ferrer atén les necessitats d'una dona amb un pit generós oferint vestits de bany moderns i elegants amb les copes B, C, D, E i F.
  <p>
    Tenim cura especial i empatia per a les dones que han estat sotmeses a cirurgia de mama. Hem inclòs una línia específica per a satisfer les necessitats d'aquestes dones que volen sentir-se còmodes i elegant al mateix temps.
  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/nferrer1.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/nferrer2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/nferrer3.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/nferrer5.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/nferrer4.jpg" alt=""/>
  </figure>

</div>