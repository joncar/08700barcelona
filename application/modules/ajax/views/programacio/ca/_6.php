<div class="ajax-gallery-title">
  <h2>Marques</h2>
</div>
<div class="col-md-6 ajax-content-text">

  <span class="projects-count">GUASCH BARCELONA
    <span>Homewear</span>
  </span>

  <div class="projects-info">

    <img src="img/graphic-designer/some-facts/guasch.jpg" alt="">
    <h4>Terrassa</h4>
    <span>C/ Blasco de Garay, 2 - Tel. +34 938 010 125</span>
    

    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
				c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
				c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
				 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
      </svg>
      <span>Des de 1859</span>
    </div>
    <div class="icon-status">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" width="32" height="30">
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
    	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
    	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
    	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
    	h24c1.1,0,2-0.9,2-2V15z"></path>
      </svg>
      <span><a href="http://guasch.es">www.guasch.es</a></span>
    </div>
  </div>

  

</div>

<div class="col-md-6 ajax-content-image">

  <p>
    Des de fa més de 150 anys a Guasch triem els millors teixits i parem esment als detalls per dissenyar i confeccionar aquelles peces que et permeten entrar en el teu propi univers.
A la teva casa, en la teva llar, de viatge, com prefereixes estar?
  </p>
  <p>
    Cada dia tries com vestir-te, com relacionar-te, on fer esport, però quan estàs amb tu mateix, també en família, com et veus?
  </p>
  <p>
    A Guasch volem que connectis amb el teu propi univers sense preocupar-te de res més. T'oferim aquest espai de confort personal a través de peces pensades exclusivament per a tu.
  </p>
  <p>
    Mocadors, pijames, boxers i vestits de bany d'home per expressar-te com vols, quan més ho necessitis, allí on et trobis.
  </p>
  <p>
    Sis generacions de la família Guasch, d'empresaris i treballadors posen cada dia a les teves mans els mocadors més delicats, que ens han donat reconeixement mundial, els pijames més còmodes i suggeridors, en els millors teixits.
També boxers i vestits de bany perquè el teu homewear més intimo i personal sigui complet.
  </p>
  <p>
    Un gran grup de professionals en un projecte sòlid, on les idees i el mercat evolucionen en un procés constant.
  </p>

  <figure>
    <img src="<?= base_url() ?>img/services/guasch3.jpg" alt=""/>
  </figure>

  
  <figure>
    <img src="<?= base_url() ?>img/services/guasch4.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/guasch1.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/guasch2.jpg" alt=""/>
  </figure>
  
  
  <figure>
    <img src="<?= base_url() ?>img/services/guasch5.jpg" alt=""/>
  </figure>

</div>