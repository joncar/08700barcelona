<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }        
        
        function read($folder,$id){
            if($folder=='team'){
                //$this->load->view('team/'.$_SESSION['lang'].'/'.$id);
                if(is_numeric($id)){
                    $gal = $this->db->get_where('galeria_equipo',array('id'=>$id));
                    if($gal->num_rows()>0){
                        $gal = $gal->row();                        
                        $this->load->view('team',array('gal'=>$gal));
                    }else{
                        throw new Exception("Marca no encontrada en nuestra bd", 404);
                        
                    }

                }
            }else{
                if(is_numeric($id)){
                    $marca = $this->db->get_where('marcas',array('id'=>$id));
                    if($marca->num_rows()>0){
                        $marca = $marca->row();
                        $page = $this->load->view('marcas',array('marca'=>$marca),TRUE);
                        echo $this->traduccion->traducir($page,$_SESSION['lang']);
                    }else{
                        throw new Exception("Marca no encontrada en nuestra bd", 404);
                        
                    }

                }
            }
        }    

        function multimedia($id){
            $multimedia = $this->db->get_where('ediciones_fotos',array('id'=>$id));
            if($multimedia->num_rows()>0){
                $this->load->view('multimedia',array('multimedia'=>$multimedia->row()));
            }
        }            
    }
