<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }                        
        function invitaciones(){
            $this->norequireds = array('nro');
            $crud = $this->crud_function('','');
            $crud->field_type('nro','hidden');
            if($crud->getParameters(FALSE)!='print' && $crud->getParameters(FALSE)!='export'){
                $crud->columns('nro','dni','nombre','apellido_1','apellido_2','email','status','fecha_aceptacion','idioma');
            }
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('-1'=>'<i class="fa fa-upload fa-2x" title="Invitación Solicitada"></i>','0'=>'<span class="label label-danger">Sin Validar</span>','1'=>'<span class="label label-success">Validado</span>'));
            }else{
                $crud->field_type('status','dropdown',array('-1'=>'Solicitada','0'=>'Sin Validar','1'=>'Validado'));
            }
            $crud->field_type('titulo','invisible')
                 ->field_type('mensaje','invisible')
                 ->field_type('idioma','enum',array('es','uk','ca'));
            $crud->order_by('id','DESC');
            $crud->callback_before_insert(function($post){
                get_instance()->load->model('querys');
                $post['nro'] = get_instance()->querys->get_key();
                return $post;
            });
            if($crud->getParameters()=='add'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[invitaciones.email]');
            }
            $crud->display_as('nro','NRO')
                 ->display_as('nombre','Nom')
                 ->display_as('apellido_1','Cognom 1')
                 ->display_as('apellido_2','Cognom 2')
                 ->display_as('status','Estatus')
                 ->display_as('fecha_aceptacion','Fecha de aceptación')
                 ->display_as('email','Email')
                 ->display_as('dni','DNI')
                 ->display_as('telefono','Telefono')
                 ->display_as('direccion','Direccion')
                 ->display_as('cp','CP')
                 ->display_as('poblacion','Població')
                 ->display_as('pais','Pais')
                 ->display_as('empresa','Empresa')
                 ->display_as('cif','CIF')
                 ->display_as('carrec','Cárrec')
                 ->display_as('web','WEB');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar invitación','',base_url('boletines/sendFromId/').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar confirmación','',base_url('invitaciones/admin/confirmar').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar mensaje','',base_url('invitaciones/admin/mensaje').'/edit/'); 
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
         function mensaje($x = ''){
            $this->as['mensaje'] = 'invitaciones';
            $crud = $this->crud_function('',''); 
            $crud->fields('titulo','mensaje','email','nombre','apellido_1','apellido_2');
            $crud->required_fields('titulo','mensaje');
            $crud->field_type('email','hidden')
                     ->field_type('nombre','hidden')
                     ->field_type('apellido_1','hidden')
                     ->field_type('apellido_2','hidden');
            if($crud->getParameters()=='list'){
                redirect(base_url('invitaciones/admin/invitaciones'));
            }
            $crud->callback_before_update(function($post){
                $this->enviarcorreo((object)$post,10);                
            });
            $crud->set_lang_string('update_success_message','Su mensaje ha sido enviado con exito');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function importar($x = '',$y = '',$z = ''){
            if($x=='procesar' && empty($z)){
                echo '<h1>¿Esta usted seguro de realizar esta acción?, esta operación sutituira los correos aún no validados y no podrá volver atras</h1>';
                echo '<p><a href="'.base_url('invitaciones/admin/importar').'">Volver Atrás</a> | <a href="'.base_url('invitaciones/admin/importar/procesar/'.$y.'/1').'">Procesar</a></p>';
            }
            elseif($x=='procesar' && $z == 1){
                $excel = $this->db->get_where('excel',array('id'=>$y))->row();
                $file = $excel->fichero;
                $idioma = $excel->idioma;
                require_once APPPATH.'libraries/Excel/SpreadsheetReader.php';
                $excel = new SpreadsheetReader('files/'.$file);
                $data = array();
                foreach ($excel as $Row)
                {
                    $data[] = $Row;
                }
                //Empresa = 0, email = 4
                //ob_start();
                if(count($data)>0){
                    //$this->db->truncate('emails');
                    //$this->db->delete('invitaciones',array('status !='=>1));
                    $this->load->model('querys');                
                    for($i=0;$i<count($data);$i++){
                        if(!empty($data[$i][0])){
                            if($this->db->get_where('boletin_email',array('email'=>$data[$i][0]))->num_rows()==0){
                                $this->db->insert('boletin_email',array('email'=>$data[$i][0],'idioma'=>$idioma));
                                echo 'Incluido '.$data[$i][0].'<br/>';
                                ob_flush();
                            }else{
                                echo $data[$i][0].' Ya existe y esta validado<br/>';
                                ob_flush();
                            }
                        }
                    }
                    echo '<a href="'.base_url('invitaciones/admin/importar').'">Volver al backend</a>';
                }else{
                    echo "Formato incorrecto";
                }
            }else{
                $this->as['importar'] = 'excel';
                $crud = $this->crud_function('','');
                $crud->set_field_upload('fichero','files');
                $crud->field_type('idioma','enum',array('es','uk','ca'));
                $crud->add_action('<i class="fa fa-refresh"></i> Procesar','',base_url('invitaciones/admin/importar/procesar').'/');
                $crud = $crud->render();

                $this->loadView($crud);
            }
        }
        
        function confirmar($id = ''){
            if(is_numeric($id)){
                $invitacion = $this->db->get_where('invitaciones',array('id'=>$id))->row();
                $this->db->update('invitaciones',array('status'=>1,'fecha_aceptacion'=>date("Y-m-d H:i:s")),array('id'=>$id));
                switch($invitacion->idioma){
                    case 'ca': $notif = '2'; break;
                    case 'es': $notif = '5'; break;
                    case 'en': $notif = '6'; break;
                } 
                $this->enviarcorreo($invitacion,$notif);
                //$this->enviarcorreo($invitacion,1,'info@080barcelonashowroom.com');
                redirect(base_url('invitaciones/admin/invitaciones/success'));
            }
        }
    }
?>
