<?= $crud->output ?>
<script>
	$(document).on('ready',function(){
		$("#url_field_box").hide();
		$("#video_field_box").hide();
	});

	$(document).on('change','#field-tipo',function(){
		var val = $(this).val();
		switch(val){
			case '1':
				$("#url_field_box").show();
				$("#video_field_box").hide();
			break;
			case '2':
				$("#url_field_box").hide();
				$("#video_field_box").show();
			break;
		}
	});
</script>