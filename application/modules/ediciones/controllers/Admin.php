<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{        
        function __construct() {
            parent::__construct();                     
        }  

        function ediciones(){
            $crud = $this->crud_function("","");    
            $crud->set_relation_n_n('Marcas','ediciones_marcas','marcas','ediciones_id','marcas_id','{idioma}-{nombre}');
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles','ca'=>'Catalan'));            
            $crud->field_type('activa','dropdown',array('-1'=>'Desactivado sin prensa','0'=>'Desactivado','1'=>'Activado'));
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        function galerias(){
            $crud = $this->crud_function("","");                
            $crud->field_type('fotos','gallery',array('path'=>'img/fashion-designer/slider','width'=>'600px','height'=>'536px')); 
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }      
        
        function marcas(){
            $crud = $this->crud_function("","");    
            $crud->set_field_upload('logo','img/marcas');   
            $crud->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles','ca'=>'Catalan'));     
            $crud->set_clone();
            $crud->add_action('Adm. Fotos','',base_url('ediciones/admin/marcas_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function marcas_fotos(){
            $this->load->library('image_crud');
            $crud = new Image_crud();
            $crud->set_table('marcas_fotos')                 
                 ->set_image_path('img/marcas')
                 ->set_relation_field('marcas_id')                 
                 ->set_url_field('foto');
            $crud->module = 'ediciones';
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ediciones_fotos(){
            $crud = $this->crud_function('','');
            $crud->field_type('tipo','dropdown',array('1'=>'Foto','2'=>'Video'));                     
            $crud->set_field_upload('url','img/multimedia')                 
                 ->display_as('url','Imagen')
                 ->set_relation_n_n('Ediciones','multimedia','ediciones','ediciones_id','ediciones_fotos_id','{nombre}')
                 ->columns('tipo','url','Ediciones')
                 ->set_clone();

            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function galeria_equipo(){
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/equipo','width'=>'970px','height'=>'970px'));
            $crud = $crud->render();            
            $this->loadView($crud);
        }
       
    }