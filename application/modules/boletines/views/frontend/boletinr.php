<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="Generator" content="Made with Mail Designer from equinux">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
        <link href="https://fonts.googleapis.com/css?family=Roboto:regular" rel="stylesheet" type="text/css" class="EQWebFont">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:regular,700" rel="stylesheet" type="text/css" class="EQWebFont">	
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; " background="http://080barcelonashowroom.com/img/boletines/page-bg.jpg">        
        <table width="100%" cellspacing="0" cellpadding="0" id="page-wrap" align="center" background="http://080barcelonashowroom.com/img/boletines/page-bg.jpg" style="">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" cellspacing="0" cellpadding="0" id="email-body" align="center">
                            <tbody>                                
                                <tr>                                    
                                    <td width="30" class="page-bg-show-thru">&nbsp;<!--Left page bg show-thru --></td>
                                    <td id="page-body" bgcolor="">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer">

                                            <div style="background-image:url(http://080barcelonashowroom.com/); color: rgb(255, 255, 255); font-family: 'Butler Stencil', Arial, 'Butler Stencil', 'Droid Sans', sans-serif; font-size: 43px; font-style: normal; font-weight: 700; line-height: 0.5; margin-bottom: 0px; margin-top: 0px; text-align: left; text-decoration: none; background-position: 0% 0%; " width="100%">
                                                <table cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="http://080barcelonashowroom.com/img/boletines/background-3.jpg" bgcolor="black" style="min-width: 100%; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="22" style="min-width: 22px; ">&nbsp;</td>
                                                            <td valign="top" align="left" style=" ">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" style=" ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" align="left" style=" ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" style=" ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" style=" ">
                                                                                                <div class="heading" style="font-size: 12px; font-family: 'Open Sans', Arial, Arial, sans-serif; color: rgb(108, 108, 108); font-style: normal; font-weight: 400; line-height: 34px; margin-bottom: 0pt; margin-top: 0pt; text-align: center; text-decoration: none; ">
                                                                                                    <font color="#7f7f7f">Si no ves bien este email haz click aquí</font> <a href="http://080barcelonashowroom.com/boletines/frontend/ver/1/{email}" style="color: #e5475f; ">AQUÍ</a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="22" style="min-width: 22px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%">
                                                <table cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table" style="min-width: 100%; ">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" class="layout-block-full-width" width="100%" style="min-width: 100%; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width" width="100%" style="min-width: 100%; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 100%; ">
                                                                                <div class="layout-block-image">
                                                                                    <a href="<?= $detail->link_banner ?>">
                                                                                        <img width="100%" height="auto" alt="" src="http://080barcelonashowroom.com/img/boletines/<?= $detail->banner ?>" border="0" style="line-height: 1px; display: block; width: 100%; height: auto; ">
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            
                                            <?php if(!empty($detail->contenido)): ?>
                                            <div style="background-image:url(http://080barcelonashowroom.com/img/boletines/background-3.jpg); background-position: 0% 0%; max-width:602px; " width="100%">
                                                <table cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="http://080barcelonashowroom.com/img/boletines/background-3.jpg" bgcolor="black" style="min-width: 100%; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                            <td  style="">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column"  style="">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" align="left" style="padding-left: 10px; padding-right: 10px;  ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" style=" ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" style=" ">
                                                                                                <div class="text" style="text-align: center; font-size: 16px; font-family: 'Lucida Grande'; ">
                                                                                                    <div style="text-align: center; ">
                                                                                                        <span style="color: rgb(94, 94, 94); font-family: Lato, Arial, 'Helvetica Neue'; font-size: 11px; "><br>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    <div style="text-align: center; ">
                                                                                                        <span style="color: rgb(94, 94, 94); font-family: Lato, Arial, 'Helvetica Neue'; font-size: 11px; "><br></span>
                                                                                                    </div>
                                                                                                    <div><br></div>
                                                                                                    <div style="color: rgb(255, 255, 255); font-family: 'Open Sans', Arial, 'Butler Stencil', 'Droid Sans', sans-serif; font-size: 11px; font-style: normal; font-weight: normal; line-height: 1.2; margin-bottom: 0px; margin-top: 0px; text-align: justify; text-decoration: none; ">
                                                                                                        <?= $detail->contenido ?>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <br>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <br>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php endif ?>
                                            <div style="background-image:url(http://080barcelonashowroom.com/img/boletines/background-3.jpg); background-position: 0% 0%; " width="100%">
                                                <table cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="http://080barcelonashowroom.com/img/boletines/background-3.jpg" bgcolor="black" style="min-width: 100%; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                            <td  style="">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column"  style="">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" align="left" style="padding-left: 10px; padding-right: 10px;  ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" style=" ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" style=" ">
                                                                                                <div class="text" style="text-align: center; font-size: 16px; font-family: 'Lucida Grande'; ">
                                                                                                    <a href="mailto: info@080barcelonashowroom.com" style="color: rgb(191, 191, 191); font-size: 11px; ">Contacto</a>&nbsp;
                                                                                                    <span style="color: rgb(94, 94, 94); font-family: Arial, 'Helvetica Neue'; font-size: 11px; ">| </span>
                                                                                                    <a href="http://080barcelonashowroom.com/paginas/frontend/unsubscribe/{email}" style="font-family: Arial, 'Helvetica Neue'; font-size: 11px; color: rgb(191, 191, 191); ">Darse de baja</a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="background-image:url(http://080barcelonashowroom.com/img/boletines/background-3.jpg); background-position: 0% 0%; " width="100%">
                                                <table cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="http://080barcelonashowroom.com/img/boletines/background-3.jpg" bgcolor="black" style="min-width: 100%; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="font-size: 1px; min-width: 11px; ">&nbsp;</td>
                                                            <td height="20"  style="">
                                                                <div class="spacer"></div>
                                                            </td>
                                                            <td width="11" style="font-size: 1px; min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                        <div style="background:white; background-position: 0% 0%; max-width:602px; " width="100%">
                                                <table cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" background="http://080barcelonashowroom.com/img/boletines/background-3.jpg" style="min-width: 100%; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                            <td  style="">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column"  style="">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" align="left" style="padding-left: 10px; padding-right: 10px;  ">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" style=" ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" style=" ">
                                                                                                <div class="text" style="text-align: center; font-size: 16px; font-family: 'Lucida Grande'; ">
                                                                                                    <div style="text-align: center; ">
                                                                                                        <span style="font-family: Lato, Arial, 'Helvetica Neue'; font-size: 11px; "><br>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    <div style="text-align: center; ">
                                                                                                        <span style="font-family: Lato, Arial, 'Helvetica Neue'; font-size: 11px; "><br></span>
                                                                                                    </div>
                                                                                                    <div><br></div>
                                                                                                    <div style="font-family: 'Open Sans', Arial, 'Butler Stencil', 'Droid Sans', sans-serif; font-size: 11px; font-style: normal; font-weight: normal; line-height: 1.2; margin-bottom: 0px; margin-top: 0px; text-align: justify; text-decoration: none; ">
                                                                                                        <?= $detail->encabezado ?>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <br>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <br>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="11" style="min-width: 11px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                    </td>
                                    <td width="30" class="page-bg-show-thru">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->
    </body>
</html>
