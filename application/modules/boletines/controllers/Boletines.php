<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Boletines extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');

        }
        function boletin(){
            $this->norequireds = array('emails');
            $crud = $this->crud_function('','');
            $emails = array();
            foreach($this->db->get('boletin_email')->result() as $e){
                $emails[] = $e->email;
            }
            $crud->set_clone();
            $crud->set_field_upload('banner','img/boletines');
            $crud->unset_columns('contenido');
            if($crud->getParameters(FALSE)=='add' || $crud->getParameters(FALSE)=='edit'){                
                $crud->display_as('emails','Coloque los emails de destinatario (Separados por coma (,)');
                $crud->display_as('excel','Importar emails desde un excel.');
            }           
            if($crud->getParameters(FALSE)=='add'){
                $crud->field_type('activado','hidden',0);
            }
            $crud->callback_column('enviados',function($val,$row){
                $enviados = explode(',',$val);
                $total = count($this->importar(strip_tags($row->excel)));
                $emails_manuales = explode(',',$row->emails);
                
                $total+= count($emails_manuales)==1 && empty($emails_manuales[0])?0:count($emails_manuales);
                if(count($enviados==1) && empty($enviados[0])){
                    return (string)'0 de '.$total;
                }else{
                    return count($enviados).' de '.$total;
                }
            });
            $crud->unset_searchs('enviados');
            $crud->set_field_upload('excel','files');
            $crud->field_type('emails','tags');
            $crud->columns('titulo','enviados','fichero','idioma','activado');
            $crud->field_type('enviados','hidden');
            $crud->field_type('idioma','enum',array('es','uk','ca','fr'));
            $crud->add_action('<i class="fa fa-envelope"></i> Añadir Cuerpo','',base_url('boletines/boletin_detalles').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Ver Boletín','',base_url('boletines/ver').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar Boletín','',base_url('boletines/send').'/');
            $action = $crud->getParameters();            
            $crud = $crud->render();
            $crud->output = $this->load->view('frontend/crud',array('output'=>$crud->output),TRUE); 
            $this->loadView($crud);
        } 
        
        function boletin_email(){            
            $crud = $this->crud_function('','');
            $crud->field_type('idioma','enum',array('es','uk','ca'));            
            $crud->field_type('status','dropdown',array('0'=>'Baja','1'=>'Activo'));
            $crud = $crud->render();                       
            $this->loadView($crud);
        }
        
        function boletin_detalles($x = ''){
            $crud = $this->crud_function('','');
            if(is_numeric($x)){
                $crud->field_type('boletin_id','hidden',$x);
                $crud->unset_columns('boletin_id','texto');
            }
            $crud->set_field_upload('foto','images/boletines');
            $crud = $crud->render();                        
            $this->loadView($crud);
        }                
        
        function ver($id = '',$email = ''){
            if(is_numeric($id)){
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = $detail->idioma=='es'?'boletinr':'boletin_'.$detail->idioma;
                $view = empty($detail->fichero)?$view:$detail->fichero;
                $cuerpo = $this->load->view('frontend/'.$view,array('detail'=>$detail),TRUE);
                $cuerpo = str_replace('{email}',$email,$cuerpo);
                echo $cuerpo;
            }                        
        }
        
        function send($id = '',$emails = ''){
            if(is_numeric($id)){
                $this->db->query("SET NAMES 'utf8'");
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();                
                $view = $detail->idioma=='es'?'boletinr':'boletin_'.$detail->idioma;
                $view = empty($detail->fichero)?$view:$detail->fichero;
                $correo = $this->load->view('frontend/'.$view,array('detail'=>$detail),TRUE);
                //$correo = $correo;
                //echo $correo;       
                ob_end_flush();
                ob_start();
                set_time_limit(0);
                if(!empty($detail->excel)){
                    $correos = $this->importar($detail->excel);                    
                }
                //Concatenamos
                if(!empty($detail->emails)){
                    $emails = explode(",",$detail->emails);
                    foreach($emails as $e){
                        $correos[] = $e;
                    }
                }
                $totalizado = !empty($correos)?count($correos):0;
                $enviados = array();
                /*if(!empty($correos)){
                    foreach($correos as $n=>$c){
                        if($n<5){                            
                            $mail = str_replace('{email}',$c,$correo);
                            $this->load->library('mailer');                    
                            //$this->mailer->mail->AddEmbeddedImage('img/boletines/'.$detail->banner,'Boletin');
                            correo($c,$detail->titulo,$mail);                            
                            $enviados[] = $c;
                            //echo "Enviado a ".$c.'<br/>';
                            //echo $mail;
                            ob_flush();
                            flush();
                        }
                    }
                }*/
                //correo('joncar.c@gmail.com',$detail->titulo,$mail);  
                $activado = $totalizado==count($enviados)?0:1;
                $this->db->update('boletin',array('enviados'=>implode(',',$enviados),'activado'=>$activado),array('id'=>$detail->id));
                echo "Se ha programado el envio del boletin <a href='javascript:history.back()'>Volver a la lista</a>";
            }                        
        }
        
        function sendFromId($x = ''){
            if(is_numeric($x)){
                $x = $this->db->get_where('invitaciones',array('id'=>$x));
                if($x->num_rows()>0){
                    if($x->row()->status==-1){
                        $this->db->update('invitaciones',array('status'=>0),array('id'=>$x->row()->id));
                    }                    
                    $this->send(1,$x->row()->email);
                }
            }
        }
        
        
        function getLastInput(){
            $this->db->order_by('id','DESC');
            $this->db->limit('1');
            $last = $this->db->get_where('boletin');
            if($last->num_rows()>0){
                echo $last->row()->texto;
            }
        }
        
        function subscritos(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function importar($x = ''){
            
            $file = $x;                        
            require_once APPPATH.'libraries/Excel/SpreadsheetReader.php';
            if(file_exists('files/'.$file)){
                $excel = new SpreadsheetReader('files/'.$file);
                $data = array();
                foreach ($excel as $Row)
                {
                    if(!empty($Row[0])){
                        $data[] = $Row[0];
                    }
                }
                return $data;
            }
            
        }
    }
?>
