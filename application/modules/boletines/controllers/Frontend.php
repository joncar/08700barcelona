<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();

        }                
        function ver($id = '',$email = ''){
            if(is_numeric($id)){
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = $detail->idioma=='es'?'boletinr':'boletin_'.$detail->idioma;
                $view = empty($detail->fichero)?$view:$detail->fichero;
                $cuerpo = $this->load->view('frontend/'.$view,array('detail'=>$detail),TRUE);
                $cuerpo = str_replace('{email}',$email,$cuerpo);
                echo $cuerpo;
            }                        
        }  
        
        function send($x = ""){
            if(empty($x)){
                $this->db->query("SET NAMES 'utf8'");
                $boletines = $this->db->get_where('boletin',array('activado'=>1));
                foreach($boletines->result() as $detail){
                    $view = $detail->idioma=='es'?'boletinr':'boletin_'.$detail->idioma;
                    $view = empty($detail->fichero)?$view:$detail->fichero;
                    $correo = $this->load->view('frontend/'.$view,array('detail'=>$detail),TRUE);
                    //$correo = $correo;
                    //echo $correo;       
                    ob_end_flush();
                    ob_start();
                    set_time_limit(0);
                    if(!empty($detail->excel)){
                        $correos = $this->importar($detail->excel);                    
                    }
                    //Concatenamos
                    if(!empty($detail->emails)){
                        $emails = explode(",",$detail->emails);
                        foreach($emails as $e){
                            $correos[] = $e;
                        }
                    }
                    if(!empty($correos)){
                        $totalizado = count($correos);

                        //Vemos a quien ya se envio
                        $enviados = $detail->enviados;
                        $enviados = explode(',',$enviados);
                        if(count($enviados)==1 && empty($enviados[0])){
                            $enviados = array();
                        }
                        foreach($correos as $n=>$c){
                            if(in_array($c,$enviados)){
                                unset($correos[$n]);
                            }
                        }                                
                        $contador = 0;
                        if(!empty($correos)){
                            foreach($correos as $n=>$c){
                                if($contador<5 && !empty($c)){
                                    $contador++;
                                    $mail = str_replace('{email}',base64_encode($c),$correo);
                                    $this->load->library('mailer');                    
                                    //$this->mailer->mail->AddEmbeddedImage('img/boletines/'.$detail->banner,'Boletin');
                                    correo($c,$detail->titulo,$mail);
                                    $enviados[] = $c;
                                    //echo "Enviado a ".$c.'<br/>';
                                    //sleep(10);
                                    ob_flush();
                                    flush();
                                }
                            }
                        }    
                        //print_r($enviados);
                        //echo $totalizado.'=='.count($enviados);
                        $activado = $totalizado==count($enviados)?0:1;
                        $this->db->update('boletin',array('enviados'=>implode(',',$enviados),'activado'=>$activado),array('id'=>$detail->id));

                        if($activado==0){
                            echo "Correo enviado a todos los contactos registrados <a href='javascript:history.back()'>Volver a la lista</a>";            
                        }else{
                            echo "Correo enviado a 5 de los contactos registrados y se programo el envio del restante <a href='javascript:history.back()'>Volver a la lista</a>";            
                        }
                    }else{
                        $this->db->update('boletin',array('enviados'=>'','activado'=>0),array('id'=>$detail->id));
                    }
                }
            }
        }
        
        function importar($x = ''){
            
            $file = $x;                        
            require_once APPPATH.'libraries/Excel/SpreadsheetReader.php';
            $excel = new SpreadsheetReader('files/'.$file);
            $data = array();
            foreach ($excel as $Row)
            {
                if(!empty($Row[0])){
                    $data[] = $Row[0];
                }
            }
            return $data;
            
        }
    }
?>
