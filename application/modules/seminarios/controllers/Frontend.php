<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();   
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('categorias_seminarios');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('seminarios',array('categorias_seminarios_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }                
        
        public function populares(){
            $seminarios = new Bdsource();
            $seminarios->limit = array('2','0');
            $seminarios->init('seminarios',FALSE,'populares');
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->link = site_url('seminaris/'.toURL($b->id.'-'.$b->titulo));
                $this->populares->row($n)->foto = base_url('img/seminarios/'.$b->foto);
            }
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->comentarios = $this->db->get_where('comentarios_seminarios',array('seminarios_id'=>$b->id))->num_rows();                
            }
            return $this->populares;
        }
        
        public function index(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('seminarios');
            $crud->set_subject('seminarios');
            $crud->set_theme('crud');
            $crud->set_url('seminarios/frontend/index/');
            $crud->columns('id','link','categorias_seminarios_id','foto','titulo','texto','user_id');
            $crud->callback_column('link',function($val,$row){
                return site_url('seminaris/'.toURL($row->id.'-'.$row->titulo));                        
            });
            $crud->callback_column('foto',function($val){
                return base_url().'img/seminarios/'.$val;                        
            });
            $crud = $crud->render('','application/modules/seminarios/views');
            $crud->crud = 'user';
            $crud->view = 'crud/main';
            $crud->populares  = $this->populares();
            $crud->categorias = $this->get_categorias();
            $crud->scripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
            $this->loadView($crud);
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $seminarios = new Bdsource();
                $seminarios->where('id',$id);
                $seminarios->init('seminarios',TRUE);
                $this->seminarios->link = site_url('seminaris/'.toURL($this->seminarios->id.'-'.$this->seminarios->titulo));
                $this->seminarios->foto = base_url('img/seminarios/'.$this->seminarios->foto);
                $this->seminarios->categorias = $this->db->get_where('categorias_seminarios',array('id'=>$this->seminarios->categorias_seminarios_id));
                $seminarios->visitas++;
                $seminarios->save();
                $comentarios = new Bdsource();
                $comentarios->where('seminarios_id',$this->seminarios->id);
                $comentarios->init('comentarios_seminarios',FALSE,'comentarios');
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('categorias_seminarios_id',$this->seminarios->categorias_seminarios_id);
                $relacionados->where('id !=',$this->seminarios->id);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('seminarios',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('seminaris/'.toURL($b->id.'-'.$b->titulo));
                    $this->relacionados->row($n)->foto = base_url('img/seminarios/'.$b->foto);
                }
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->seminarios,
                        'title'=>$this->seminarios->titulo,
                        'comentarios'=>$this->comentarios,
                        'categorias'=>$this->get_categorias(),
                         'populares'=>$this->populares(),
                        'relacionados'=>$this->relacionados
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('seminarios_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios_seminarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('seminarios/frontend/read/'.$_POST['seminarios_id'].'#respond'));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('seminarios/frontend/read/'.$_POST['seminarios_id'].'#respond'));                
            }
        }
    }
?>
