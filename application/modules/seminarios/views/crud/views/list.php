<?php if(!empty($list)): ?>
    <?php foreach($list as $num_row => $d): ?>    
        <article class="post">
            <div class="entry-image">
                <?php if(!empty($d->foto)): ?>
                    <img src="<?= $d->foto ?>" alt="image" style="width:100%">
                <?php endif ?>
            </div>
            <div class="content-post">
                <div class="title-education">
                    <p><?= $d->sbfa7a2a4 ?></p>
                </div>
                <h4 class="title-post">
                    <a href="<?= $d->link ?>"><?= $d->titulo ?></a>
                </h4>
                <div class="entry-meta">                              
                    <span class="author"><a href="#"><?= $d->user_id ?></a></span>
                    <span class="date"><a href="#"><?= ucfirst(strftime("%d-%m-%Y",strtotime($d->fecha))); ?></a></span>
                    <span class="comment"><a href="#">0 Comentaris</a></span>
                </div><!-- /.entry-meta -->

                <div class="entry-content">
                    <p><?= substr(strip_tags($d->texto),0,255).'...' ?></p>
                </div><!-- /entry-post -->
                <div class="more">
                    <a href="<?= $d->link ?>">Llegir Més</a>
                </div>
                <ul class="flat-socials">
                    <li class="facebook">
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="twitter">
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>

                </ul>
            </div><!-- /content-post -->
        </article><!-- /post -->
    <?php endforeach ?>     
<?php endif; ?>
