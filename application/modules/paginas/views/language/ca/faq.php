<h2>FAQ</h2>
<section class="accordeon">
    <!-- 
<article>
        <header>
            <h4>¿Cómo llegar hasta el 080 Barcelona Fashion Showroom?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <!~~ 
<figure class="left">
                    <img src="<?= base_url() ?>img/faq/1.jpg" alt=""/>
                </figure>
                ~~>
                <p>
                    Sant Pau Recinte Modernista<br>
                    C. Sant Antoni Maria Claret, 167 (esquina C. Cartagena)<br>
                    08025 Barcelona<br>
                    Metro (L5): Sant Pau – Dos de Maig<br>
                    Bus: H8, 19, 20, 45, 47, 50, 51, 92, 117, 192

                    <iframe marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.es/maps?f=q&amp;source=s_q&amp;hl=ca&amp;geocode=&amp;q=Recinte+modernista+Sant+Pau&amp;aq=&amp;sll=41.692248,1.745867&amp;sspn=3.921187,7.7948&amp;ie=UTF8&amp;hq=Recinte+modernista&amp;hnear=St+Pau,+Carrer+Nou,+08395+Sant+Pol+de+Mar,+Barcelona&amp;ll=41.411416,2.174374&amp;spn=0.030767,0.060897&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=2007392049700870452&amp;output=embed" frameborder="0" height="400" width="630"></iframe>
                </p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                  C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
 -->
    <article>
        <header>
            <h4>QUÈ ÉS EL 080 BARCELONA SHOWROOM?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/que es el showroom.jpg" alt=""/>
                </figure>
                <p>
                    El 080 Barcelona Showroom connecta a dissenyadors i marques de Barcelona amb compradors, agents o distribuïdors a nivell mundial.

 </p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
    <article>
        <header>
            <h4>QUÈ ÉS EL 080 BARCELONA FASHION?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/faq/3.jpg" alt=""/>
                </figure>
                <p>
                    És una passarel·la internacional que presenta cada temporada col·leccions de més de 30 marques i dissenyadors catalans. Amb més de 40.000 professionals i 500 mitjans internacionals acreditats han participat en cada edició.</p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
    <article>
        <header>
            <h4>COM PARTICIPAR EN EL 080 BARCELONA FASHION SHOWROOM DIGITAL EDITION?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/080 como participar.jpg" alt=""/>
                </figure>
                <p>
                    Aquesta edició del 080 Barcelona Showroom consta d'una plataforma digital per a professionals de la moda. La plataforma és una comunitat privada i exclusiva. Has de registrar-te per a poder accedir a la comunitat i tots els seus beneficis.</p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
    <!-- 
<article>
        <header>
            <h4>¿Cuáles son los hoteles más próximos al showroom?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/faq/5.jpg" alt=""/>
                </figure>
                <p>
                    Barcelona es una ciudad cosmopolita y atractiva para visitantes profesionales. Debido a su vocación turística, es fácil encontrar hoteles en prácticamente todos los barrios del centro de la ciudad.

                    Tiene una gran oferta en hoteles y los traslados entre los diferentes barrios de la ciudad no son largos, ya sea en metro o en taxi. De todos modos, adjuntamos abajo información sobre hoteles situados en la zona del evento (Área de la Sagrada Familia y Sant Pau).
                </p>
                <p><a href="http://www.hotelsantpau.com"> Hotel Sant Pau</a></p>
                <p><a href="http://hotel-medicis-barcelona.h-rez.com/index.htm?lbl=ggl-en&gclid=CjwKEAjw9MrIBRCr2LPek5-h8U0SJAD3jfht1S3egHP03aMewHag5Vxsu8dAFKmGJHWc7apEeVDzmhoC9Ezw_wcB"> Hotel Medicis Barcelona</a></p>
                <p><a href="http://www.hotusa.es/hoteles/espana/barcelona/barcelona/hotel-medium-aristol.html?gclid=CjwKEAjw9MrIBRCr2LPek5-h8U0SJAD3jfhtSF5hQ0qo0KTTHFqtPSZWlzZwsk7tuELn-wbpAEoL7hoC6Bnw_wcB"> Hotusa</a></p>
                <p><a href="http://www.ilunionbelart.com/?gclid=CjwKEAjw9MrIBRCr2LPek5-h8U0SJAD3jfhtv-gej7AQvSz6m2-h-UYrBWiVg-UzT5ly0UKaGpgGRxoC8Cfw_wcB"> Iluniobelart</a></p>
                
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
 -->
    <article>
        <header>
            <h4>QUAN PUC ACCEDIR A la COMUNITAT DIGITAL?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/como puedo acceder comunidad digital.jpg" alt=""/>
                </figure>
                <p>
            Després del registre, la plataforma està disponible del 10 de setembre al 23 d'octubre del 2020. Durant aquest temps pots accedir a la comunitat.</p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
    <article>
        <header>
            <h4>EN QUINES ACTIVITATS DE LA 080 BARCELONA FASHION -SETMANA DE LA MODA PUC PARTICIPAR?</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/en que actiividad.JPG" alt=""/>
                </figure>
                <p>
            En la pàgina oficial del 080 Barcelona Fashion pots accedir a l’agenda, dissenyadors i tota la informació d'aquesta edició digital.</p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>
    <article>
        <header>
            <h4>NO SÓC PROFESSIONAL, PERÒ AIXÍ I TOT VULL VISITAR LA 080 BARCELONA FASHION</h4>
        </header>
        <div class="accordeon-item">
            <div class="accordeon-content">
                <figure class="left">
                    <img src="<?= base_url() ?>img/faq/1.jpg" alt=""/>
                </figure>
                <p>
            El 080 Barcelona Fashion Showroom és un espai privat per a professionals del sector. No obstant,  pots accedir a 080 Barcelona Digital edition.</p>
            </div>
        </div>
        <button class="accordeon-btn">
            <svg version="1.1" width="10px" height="10px" viewbox="0 0 450 250" enable-background="new 0 0 450 250" xml:space="preserve" fill="#ffffff">
            <path
                d="M8.738,50.005L207.8,242.35c11.369,10.996,29.5,10.678,40.479-0.683
                c11.003-11.358,10.705-29.485-0.664-40.481L48.556,8.842C37.186-2.153,19.055-1.835,8.052,9.523
                C-2.951,20.882-2.631,39.01,8.738,50.005z"/>
            <path
                d="M213.039,201.126L403.533,8.546c10.891-11.008,28.247-10.69,38.765,0.682
                c10.518,11.395,10.232,29.522-0.657,40.531L251.146,242.338c-10.891,11.01-28.249,10.691-38.765-0.682
                C201.86,230.284,202.146,212.135,213.039,201.126z"/>
            </svg>
        </button>
    </article>

</section>
