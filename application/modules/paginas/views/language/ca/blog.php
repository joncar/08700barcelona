<div class="content row blog-head">
	<div class="col-md-6">
		<h2>Premsa</h2>
		<div class="user-area" data-action="sign-in">
			
		</div>
	</div>
</div>
<?php foreach($blog->result() as $b): ?>
<!-- Photo post -->
<div class="content row blog-post" data-action="show-post" data-src="blog/<?= toURL($b->id.'-'.$b->titulo) ?>">
	<img src="<?= $b->foto ?>" class="blog-post-bg" alt=""/>
	<header>
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 32 32" style="enable-background:new 0 0 32 32;" xml:space="preserve">
			<path
				style="fill-rule:evenodd;clip-rule:evenodd;"
				d="M27.817,32H3.974C1.779,32,0,30.219,0,28.024V4.164C0,1.968,1.805,0,4,0h16v2
				H4C2.903,2,1.987,3.066,1.987,4.164v23.859c0,1.098,0.89,1.988,1.987,1.988h23.843C28.914,30.012,30,29.098,30,28V12h2v16
				C32,30.196,30.012,32,27.817,32z M15.941,22.364c-0.291,0.291-0.654,0.436-1.03,0.509l-8.7,4.303
				c-0.979,0.455-1.763-0.424-1.403-1.403l4.3-8.706c0.073-0.375,0.218-0.739,0.509-1.03L25.074,0.573c0.776-0.776,2.034-0.776,2.81,0
				l3.512,3.515c0.776,0.776,0.776,2.036,0,2.812L15.941,22.364z M6.713,24.571c-0.227,0.459,0.212,0.929,0.701,0.702l5.372-3.254
				l-2.822-2.823L6.713,24.571z M11.726,18.147l2.108,2.109c0.388,0.388-0.615-0.615,0.702,0.703l11.24-11.247l-3.537-3.49
				L11.024,17.444C11.397,17.817,11.339,17.759,11.726,18.147z M29.289,4.791l-2.107-2.109c-0.388-0.388-1.017-0.388-1.405,0
				L23.719,4.74l3.462,3.565l2.107-2.109C29.677,5.808,29.677,5.179,29.289,4.791z"/>
			</svg>
		</header>
		<div class="col-md-5 content-table">
			<footer class="content-cell-bottom">
				<h3><?= $b->titulo ?></h3>
				<span class="post-info">
					<a href="#">
						<time datetime="2015-12-24"><?= strftime('%d %m %Y',strtotime($b->fecha)) ?></time>
					</a>
					/
					<a href="#"><?= $b->user ?></a>
				</span>
				<span class="post-tag">
					<?php if(!empty($b->tags)): ?>
					<?php foreach(explode(',',$b->tags) as $t): ?>
					<a href="#"><?= $t ?></a>/
					<?php endforeach ?>
					<?php endif ?>
					
				</span>
			</footer>
		</div>
	</div>
	<!-- / Photo post -->
	<?php endforeach ?>