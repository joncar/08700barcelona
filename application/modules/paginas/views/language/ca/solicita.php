<div class="col-md-5">
    <h2>Demanar Acreditació</h2>
    <p>El 080 Barcelona Showroom és un espai exclusiu per a professionals del sector de la Moda.
        Si ets professional del sector (Comprador, Agent Comercial, Distribuïdor, Comerç de Moda o anàleg) pots sol·licitar la teva acreditació professional omplint i enviant-nos el formulari que trobaràs a continuació. En un màxim de 48 hores rebràs, a l’adreça de correu electrònic facilitada, la confirmació de la teva acreditació. Cada acreditació és personal i intransferible.
        <br><br>Durant els dies 4 i 5 de Febrer de 2019, podràs recollir la teva acreditació personal al mostrador d’acreditacions del recinte modernista de l’antic Hospital de Sant Pau de Barcelona (c/Sant Quintí, 89. 08026. Barcelona) de les 10h del mati a les 18h de la tarda.<br><br>
    </p>                        
    <?= $solicitar ?>
</div>
