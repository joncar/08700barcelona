<div class="col-md-5">
    <h2 style="margin-bottom: 0;">080 barcelona showroom?</h2>
    <h3 style="font-size: 1.428rem;"> 10/09/20 – 23/10/20 Digital Showroom Edition</h3>

    <h3>ESTIL MEDITERRANI</h3>
    <p>
        El disseny de moda és un fenomen cultural lligat a un territori. Les persones que viuen en ell, 
        la seva personalitat col·lectiva, les seves tradicions, els usos socials, la climatologia 
        i les condicions de vida d’aquest territori impregnen el disseny i la creació de les col·leccions 
        fins a definir un estil propi. 
    </p>
    <p style=" margin-bottom: 2.5em">
        Barcelona és una important capital del mediterrani. L’hospitalitat, la llibertat creativa, 
        la interculturalitat i el respecte a la diversitat son valors que descriuen a aquest territori, 
        acollidor amb les idees i les persones. El modernisme, com a personalitat arquitectònica de Barcelona, 
        és bona síntesi entre la identitat pròpia de la ciutat i la seva vessant cosmopolita. 
    </p>
     <div><button target="mainFrame" onclick="document.location.href='<?= base_url('files/que-es/'.$this->db->get('ajustes')->row()->{'que_es_'.$_SESSION['lang']}) ?>'">+INFO</button></div>

    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-6 padding-0">
            <ul class="gallery no-padding-li-sm row" data-item="6">
                
                <?php
                    //Deshabilitado para poner el video
                    $this->db->order_by('orden','ASC'); 
                    foreach($this->db->get_where('galeria_equipo',['id'=>-1])->result() as $g): ?>
                    <li class="col-xs-2 col-sm-2">
                        <a href="#about" data-action="ajax/frontend/read/team/<?= $g->id ?>">
                            <img src="<?= base_url() ?>img/equipo/<?= $g->foto ?>" alt="">
                        </a>
                    </li>
                <?php endforeach ?>
                <!--<li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-2">
                        <img src="<?= base_url() ?>img/team/worker-elements-2.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-3">
                        <img src="<?= base_url() ?>img/team/worker-elements-3.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-4">
                        <img src="<?= base_url() ?>img/team/worker-elements-4.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-5">
                        <img src="<?= base_url() ?>img/team/worker-elements-5.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-6">
                        <img src="<?= base_url() ?>img/team/worker-elements-6.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-7">
                        <img src="<?= base_url() ?>img/team/worker-elements-7.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-8">
                        <img src="<?= base_url() ?>img/team/worker-elements-8.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-9">
                        <img src="<?= base_url() ?>img/team/worker-elements-9.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-10">
                        <img src="<?= base_url() ?>img/team/worker-elements-10.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-11">
                        <img src="<?= base_url() ?>img/team/worker-elements-11.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-12">
                        <img src="<?= base_url() ?>img/team/worker-elements-12.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-13">
                        <img src="<?= base_url() ?>img/team/worker-elements-13.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-14">
                        <img src="<?= base_url() ?>img/team/worker-elements-14.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-15">
                        <img src="<?= base_url() ?>img/team/worker-elements-15.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-16">
                        <img src="<?= base_url() ?>img/team/worker-elements-16.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-17">
                        <img src="<?= base_url() ?>img/team/worker-elements-17.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-18">
                        <img src="<?= base_url() ?>img/team/worker-elements-18.jpg" alt="">
                    </a>
                </li>
                <li class="col-xs-2 col-sm-2">
                    <a href="#about" data-action="ajax/frontend/read/team/project-9">
                        <img src="<?= base_url() ?>img/team/worker-elements-19.jpg" alt="">
                    </a>
                </li>-->
            </ul>
        </div>
        <div class="col-xs-12 padding-0">
            <iframe width="560" height="315" style="width:100%" src="https://www.youtube.com/embed/fxRZfOEPMFI?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>    
    <p>
        El valor de la feina ben feta, el talent creatiu, la innovació com activitat quotidiana 
        i el mestissatge per actitud ajuden a definir les col·leccions de les marques de moda del mediterrani. 
    </p>

    <p>
        Si visites el 080 Barcelona Fashion Showroom, trobaràs un espai en el qual es mostren les noves col·leccions de marques que representen aquest estil a professionals del sector.  
    </p>

    <p style="margin-bottom: 2em;">
        El 080 Barcelona Fashion Showroom es realitzarà en el mateix espai i en coordinació amb la Setmana de la Moda de Barcelona: 080 Barcelona Fashion en la que desfilaran més de 30 marques i dissenyadors de prestigi internacional.  
    </p>

   </div>
