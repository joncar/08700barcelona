<div class="col-md-5">
    <h2>REQUEST YOUR ACCREDITATION</h2>
    <p>The 080 Barcelona Showroom is a space exclusively designed for professionals in the fashion sector.
        If you are professional in the sector (Buyer, Commercial Agent, Distributor, Fashion Retailer or similar) you can request for accreditation filling out the form you can find below. In a maximum of 48 hours you will receive, at the email address provided, the confirmation of your accreditation.  Each accreditation is personal and non-transferable.
        <br><br>During 4th and 5th Februaruy 2019, you will be able to pick up your personal accreditation at the accreditation desk of the Recinto Modernista de Sant Pau (c/Sant Quintí, 89. 08026. Barcelona) from 10:00 am to 18:00 pm.<br><br>
    </p>                        
    <?= $solicitar ?>
</div>
