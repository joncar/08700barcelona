<div class="col-md-5">
    <h2 style="margin-bottom: 0;">080 barcelona showroom?</h2>
    <h3 style="font-size: 1.428rem;"> 10/09/20 – 23/10/20 Digital Showroom Edition </h3>

    <h3>MEDITERRANEAN STYLE</h3>
    <p>
        Fashion design is a cultural phenomenon linked to a territory. The people who live there, their collective personality, traditions, social uses, climatology and living conditions of that territory allowed the design and creation of the collections with their own style. 
    </p>
    <p style=" margin-bottom: 2.5em">
        Barcelona is an important capital of the Mediterranean: hospitality, creative freedom, interculturality and respect for diversity are values ​​that define this territory, welcoming with ideas and people. Modernism, as Barcelona's architectural personality, is a good synthesis between the city's own identity and its cosmopolitan side. 
    </p>
    <div><button target="mainFrame" onclick="document.location.href='<?= base_url('files/que-es/'.$this->db->get('ajustes')->row()->{'que_es_'.$_SESSION['lang']}) ?>'">+INFO</button></div>
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-6 padding-0">
            <ul class="gallery no-padding-li-sm row" data-item="6">
                <?php
                    //Deshabilitado para poner el video
                    $this->db->order_by('orden','ASC'); 
                    foreach($this->db->get_where('galeria_equipo',['id'=>-1])->result() as $g): ?>
                    <li class="col-xs-2 col-sm-2">
                        <a href="#about" data-action="ajax/frontend/read/team/<?= $g->id ?>">
                            <img src="<?= base_url() ?>img/equipo/<?= $g->foto ?>" alt="">
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="col-xs-12 padding-0">
            <iframe width="560" height="315" style="width:100%" src="https://www.youtube.com/embed/fxRZfOEPMFI?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    <p>
        The value of a job well done, creative talent, innovation as a daily activity and miscegenation as attitude, help define the collections of Mediterranean fashion brands. 
    </p>

    <p>
        If you came to visit the 080 Barcelona Fashion Showroom, you will find an space in which professionals from the industry will be able to know the New Collection collections of brands that shows the Mediterranean style. 
    </p>

    <p style="margin-bottom: 2em;">
        The 080 Barcelona Fashion Showroom will take place during the Barcelona Fashion Week: 080 Barcelona Fashion in which more than 30 brands and designers of international prestige will exhibit their collections.
    </p>
    
</div>
