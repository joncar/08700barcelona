<h2>EXHIBITORS</h2>
<p>
    Below you can find a list of some of our previous exhibiting brands.
</p>
<div class="row" style="margin:20px; display:none">
    <?php 
        $this->db->order_by('id','ASC');
        $ediciones = $this->db->get_where('ediciones',array('ediciones.idioma'=>$_SESSION['lang'],'activa'=>0));            
        foreach($ediciones->result() as $e):
    ?>
        <button class="edicion" data-id="<?= $e->id ?>"><?= $e->temporada ?></button>
    <?php endforeach ?>
</div> 
<br>
<?php
    foreach($ediciones->result() as $n=>$ed):
    $this->db->select('ediciones.nombre, marcas.nombre as marca, marcas.id as marcaid, marcas.logo');
    $this->db->order_by('marcas.nombre','ASC');
    $this->db->join('marcas','marcas.id = ediciones_marcas.marcas_id');
    $this->db->join('ediciones','ediciones.id = ediciones_marcas.ediciones_id');
    $this->db->group_by('marcas.nombre');
    $expositors = $this->db->get_where('ediciones_marcas',array('ediciones.id'=>$ed->id,'ediciones.idioma'=>$_SESSION['lang'],'activa'=>0));
    if($expositors->num_rows()>0): 
?>
<div class="col-md-6">
    <ul class="gallery row gallery-services <?= $n==0?'':'ocultar' ?> gal<?= $ed->id ?>"" data-item="7">
        <?php foreach($expositors->result() as $e): ?>
            <li class="col-sm-3">
                <a href="#expositors" data-action="ajax/frontend/read/programacio/<?= $e->marcaid ?>">
                    <div style="background: url(<?= base_url('img/marcas/'.$e->logo) ?>); background-position:center; background-size:cover;" class="expositorsLogo"></div>
                    <span>
                        <?= $e->marca ?>
                    </span>
                    <p></p>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; endforeach ?>