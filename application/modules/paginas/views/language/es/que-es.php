<div class="col-md-5">
    <h2 style="margin-bottom: 0;">080 barcelona showroom?</h2>
    <h3 style="font-size: 1.428rem;"> 10/09/20 – 23/10/20 Digital Showroom Edition  </h3>

    <h3>ESTILO MEDITERRÁNEO</h3>
    <p>
        El diseño de moda es un fenómeno cultural indisociablemente unido a un territorio. Las personas que viven en él, 
        su personalidad colectiva, sus tradiciones, sus usos sociales, la climatología y las condiciones de vida de dicho territorio
        impregnan el diseño y la creación de las colecciones hasta lograr un estilo propio.
    </p>
    <p style=" margin-bottom: 2.5em">
        Barcelona es una importante capital del Mediterráneo, la hospitalidad, la libertad creativa, 
        la interculturalidad y el respeto a la diversidad son valores que definen a éste territorio, 
        acogedor con las ideas y las personas. El modernismo, como personalidad arquitectónica de Barcelona, 
        es una buena síntesis entre la identidad propia de la ciudad y su vertiente cosmopolita.  
    </p>
     	<div><button target="mainFrame" onclick="document.location.href='<?= base_url('files/que-es/'.$this->db->get('ajustes')->row()->{'que_es_'.$_SESSION['lang']}) ?>'">+INFO</button></div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-6 padding-0">
            <ul class="gallery no-padding-li-sm row" data-item="6">
                <?php
                    //Deshabilitado para poner el video
                    $this->db->order_by('orden','ASC'); 
                    foreach($this->db->get_where('galeria_equipo',['id'=>-1])->result() as $g): ?>
                    <li class="col-xs-2 col-sm-2">
                        <a href="#about" data-action="ajax/frontend/read/team/<?= $g->id ?>">
                            <img src="<?= base_url() ?>img/equipo/<?= $g->foto ?>" alt="">
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="col-xs-12 padding-0">
            <iframe width="560" height="315" style="width:100%" src="https://www.youtube.com/embed/fxRZfOEPMFI?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>    
    <p>
        El valor por el trabajo bien hecho, el talento creativo, la innovación como actividad cotidiana 
        y el mestizaje como actitud ayudan a definir las colecciones de las marcas de moda mediterráneas. 
    </p>

    <p>
        Si visitas el 080 Barcelona Fashion Showroom, encontrarás un espacio en el cual se muestra, 
        a profesionales del sector, la Nueva Colección de marcas que encarnan el estilo Mediterráneo.  
    </p>

    <p style="margin-bottom: 2em;">
        El 080 Barcelona Fashion Showroom tendrá lugar durante la Semana de la Moda de Barcelona: La 080 Barcelona Fashion en la cual desfilarán más de 30 marcas y diseñadores de prestigio internacional.   
    </p>
    
</div>
