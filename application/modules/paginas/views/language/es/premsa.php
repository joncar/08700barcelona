<h2>PRENSA</h2>
<h3>
    This is what we are passionate about
</h3>
<p>
    Fashion designers see the world around as a non-stop source of inspiration. That gives them instant ideas of fascinating products.
</p>
<br>

<div class="row">
    <div class="col-xs-12 col-sm-5 col-md-6 padding-0">
        <ul class="gallery row" data-item="3">
            <li class="col-xs-2 col-sm-6 col-md-4 col-lg-4">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_1">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-1.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-3 col-md-2 col-lg-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_2">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-2.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-3 col-md-2 col-lg-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_3">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-3.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_4">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-4.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_5">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-5.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_6">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-6.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_7">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-7.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_8">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-8.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_9">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-9.jpg" alt="">
                </a>
            </li>
            <li class="col-xs-2 col-sm-2">
                <a href="#galeria" data-action="ajax/frontend/read/galeria/_10">
                    <img src="<?= base_url() ?>img/fashion-designer/fashion-element-10.jpg" alt="">
                </a>
            </li>
        </ul>
    </div>
</div>
