<div class="col-xs-12 col-md-5">
    <h2>Solicita tu acreditación</h2>
    <p>
        El 080 Barcelona Showroom es un espacio destinado exclusivamente a profesionales del sector de la Moda. 
        Si eres profesional del sector (Comprador, Agente Comercial, Distribuidor, Comercio de Moda o análogo) puedes solicitar
        tu acreditación profesional rellenado y enviándonos el formulario que hallarás a continuación. En un máximo de 48 horas recibirás, 
        en la dirección de correo facilitada, la confirmación de tu acreditación. Cada acreditación es personal e intransferible.
        <br><br>Durante los días 4 y 5 de Febrero de 2019, podrás recoger tu acreditación personal en el mostrador de acreditaciones del recinto modernista del antiguo
        Hospital de Sant Pau de Barcelona (c/Sant Quintí, 89. 08026. Barcelona) de las 10h de la mañana a las 18h de la tarde.<br><br>
    </p>                        
    <?= $solicitar ?>
</div>
