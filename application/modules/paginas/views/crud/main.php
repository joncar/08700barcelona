<div class="page-title parallax parallax4 projectes"  style=' background-size: inherit;'>           
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <div class="page-title-heading">
                        <h2 class="title">Projectes</h2>
                    </div><!-- /.page-title-heading -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><a href="#">Home</a></li>
                            <li>Projectes</li>
                        </ul>                   
                    </div><!-- /.breadcrumbs --> 
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->                      
    </div><!-- /page-title parallax -->

    <section class="main-content blog-post v1">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="post-wrap">                        
                        <?= $output ?>                     
                    </div><!-- /post-wrap -->
                </div><!-- /col-md-8 -->

                <div class="col-md-4">
                    <div class="sidebar">
                        <div class="widget widget-search">
                            <form action="<?= base_url('projectes') ?>" id="searchform" method="get">
                                <div>
                                    <input type="text" id="s" name="direccion" class="sss" placeholder="Buscar">
                                    <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
                                    <input type="hidden" id="categorias_proyectos_id" name="categorias_proyectos_id" value="<?= !empty($_GET['categorias_proyectos_id'])?$_GET['categorias_proyectos_id']:'' ?>">
                                    <input type="submit" value="" id="searchsubmit">
                                </div>
                            </form>
                        </div><!-- /widget-search -->

                        <div class="widget widget-recent-posts">
                            <h5 class="widget-title">Projectes Populars</h5>
                            <ul class="recent-posts clearfix">
                                
                                <?php foreach($populares->result() as $d): ?>
                                    <li>
                                        <div class="thumb">
                                            <img src="<?= $d->foto ?>" alt="image" style="width:84px">
                                        </div>
                                        <div class="text">
                                            <a href="<?= $d->link ?>"><?= $d->titulo ?></a>
                                            <p><?= ucfirst(strftime("%d/%m/%Y",strtotime($d->fecha))); ?></p>
                                        </div>
                                    </li>
                                <?php endforeach ?> 
                            </ul><!-- /popular-news clearfix -->
                        </div><!-- /widget widget-recent-posts -->

                        <div class="widget widget-categories">
                            <h5 class="widget-title">Categories</h5>
                            <ul class="categories">
                                
                                <?php if($categorias->num_rows()==0): ?>
                                    <li>
                                        <a href="#">Sense categories</a>
                                    </li>
                                <?php endif ?>
                                <?php foreach($categorias->result() as $c): ?>
                                    <li><a href="<?= site_url('projectes?categorias_proyectos_id='.$c->id) ?>"><?= $c->nombre ?> (<?= $c->cantidad ?>)</a></li>
                                <?php endforeach ?>
                                
                            </ul>
                        </div><!-- /widget-categories -->

                        <div class="widget widget-tags">
                            <h5 class="widget-title">Tags</h5>
                            <ul class="tag-list">
                                <?php if(!empty($detail->tags)): ?>
                                    <?php foreach(explode(',',$detail->tags) as $t): ?>
                                        <li><a href="<?= base_url('proyecto?direccion='.str_replace('#','',$t)) ?>" class="tag-link"><span><?= $t ?></span></a></li>
                                    <?php endforeach ?>            
                                <?php endif ?>
                            </ul>
                        </div><!-- /widget-tags -->
                    </div><!-- /sidebar -->
                </div><!-- /col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section><!-- /main-content blog-post -->
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchform").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchform").submit();
    }
</script>
