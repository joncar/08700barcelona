 <main>
    <!-- HOME PAGE -->
    <section class="cospo-page home-page no-bg" id="home" style="width: 985px;<!-- background-color: rgba(0, 0, 0, 0.50) -->;">
        <div class="background">
            <div class="half-width">
                <img src="<?= base_url() ?>img/fashion-designer/bg/bg-1.jpg" alt="">
            </div>
            <ul class="slider slider-small slider-small-top slider-small-right slider-translate">
                
                <?php foreach(explode(',',$this->db->get_where('galerias',array('id'=>1))->row()->fotos) as $f): if(!empty($f)): ?>
                <li class="item">
                    <img src="<?= base_url() ?>img/fashion-designer/slider/<?= $f ?>" alt="">
                </li>
                <?php endif; endforeach ?>
            </ul>
            <ul class="slider slider-small slider-small-bottom slider-small-right slider-translate">
                <?php foreach(explode(',',$this->db->get_where('galerias',array('id'=>2))->row()->fotos) as $f): if(!empty($f)): ?>
                <li class="item">
                    <img src="<?= base_url() ?>img/fashion-designer/slider/<?= $f ?>" alt="">
                </li>
                <?php endif; endforeach ?>
            </ul>
        </div>
        <div class="home-page-logo">
            <div class="idiomalogomain row hidden-xs">
                <div class="col-md-3">
                    <a href="<?= base_url('main/traducir/ca') ?>" class="<?= $_SESSION['lang']=='ca'?'active':'' ?>">CAT</a>
                    <a href="<?= base_url('main/traducir/es') ?>" class="<?= $_SESSION['lang']=='es'?'active':'' ?>">ESP</a> 
                    <a href="<?= base_url('main/traducir/en') ?>" class="<?= $_SESSION['lang']=='en'?'active':'' ?>">ENG</a>                 
                </div>
            </div>
            <a href="<?= base_url() ?>">
                <img src="<?= base_url() ?>img/svg/logo.svg" alt="">
            </a>
            <h1 style="margin-top:18px; font-size: 1.06rem;color: #fff;">Per a professionals del sector de la moda</h1>
        </div>
        <div class="content row">
            <div class="wrapper-scroll fixed-scroll">
                <div class="scroll">
                    <div class="content-center">
                        <div>
                            <div class="col-md-3">
                                <h1 style="margin-top:0px; font-size: 1.9rem;letter-spacing: .04em;color: #fff;">
                                  <?php echo $this->db->get('ajustes')->row()->{'fecha_'.$_SESSION['lang']}; ?>
                                  <p style="margin-top:0px;font-size: 1.6rem;letter-spacing: .04em;color: #fff;">
                                    DIGITAL SHOWROOM EDITION
                                  </p>
                                </h1>
                                <h1 style="margin-bottom: .21em;color: #fff;">REGISTRA'T</h1>
                                <p style="font-weight: 400;font-size: 1.05em;letter-spacing: 0.03em;color: #fff;">
                                El 080 Barcelona Fashion Showroom, un espai en el qual les millors marques d’estil mediterrani et mostraran les seves col·leccions en el marc de la passarel·la 080 Barcelona Fashion.  
                                </p>
                                <a style="font-size:1em;" href="<?= $this->db->get('ajustes')->row()->{'acreditacion_'.$_SESSION['lang']} ?>" target="_new"><button style=" color: #fff;">Registra't</button></a>
                                <!--<a style="font-size:1em;" href="mailto:info@modacc.cat" target="_new"><button style=" color: #fff;"></button></a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="home-page-footer" >
            <div class=" col-md-3" style="padding-left:0px;">
                <p style="font-size:14px; margin-bottom: 1px; color: #fff;">Organitza</p>
                <img src='<?= base_url() ?>img/svg/logos.svg' style="width:65%;">
                <br>
                <!-- <p style="line-height:36px;font-size:1.2rem;"> RESTEN <span id="getting-started" class="countdown-variant" style="font-size:1.2rem; font-family: Dosis;"></span></p> -->
                <p style=" font-size:1.2rem; font-family: Dosis;color: #fff;">© 080 barcelona showroom, 2020</p>
            </div>
        </footer>
    </section>
    <!-- END HOME PAGE -->
    <!-- ABOUT PAGE -->
    <section class="cospo-page" id="que-es">
        <div class="background">
              <div class="pattern"></div>
              <video autoplay="" muted="" loop="">
                <source src="<?= base_url() ?>video/video-maker.ogv" type="video/ogg">
                <source src="<?= base_url() ?>video/video-maker.webm" type="video/webm">
                <source src="<?= base_url() ?>video/video-maker.mp4" type="video/mp4">
              </video>
        </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content row">
                    <?php $this->load->view('language/'.$_SESSION['lang'].'/que-es'); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- END ABOUT PAGE -->
    <!-- BLOG PAGE -->
     <section class="cospo-page" id="premsa">
        <div class="background">
              <div class="pattern"></div>
              <video autoplay="" muted="" loop="">
                <source src="<?= base_url() ?>video/video-maker.ogv" type="video/ogg">
                <source src="<?= base_url() ?>video/video-maker.webm" type="video/webm">
                <source src="<?= base_url() ?>video/video-maker.mp4" type="video/mp4">
              </video>
        </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content row">
                    <?php $this->load->view('multimedia'); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- END BLOG PAGE -->
    <!-- SERVICES PAGE -->
    <section class="cospo-page" id="expositors">
        <div class="background">
            <img src="<?= base_url() ?>img/restaurant/bg/bg-6.jpg" alt="">
        </div>
        <div class="background-rightside">
              <img src="<?= base_url() ?>img/restaurant/bg/bg-6.jpg" alt="">
        </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content row">
                    <div class="col-md-5">
                        <?php $this->load->view('language/'.$_SESSION['lang'].'/expositors'); ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- SERVICES PAGE -->
    <section class="cospo-page" id="proxima">
        <div class="background">
            <img src="<?= base_url() ?>img/agency/bg/bg-3_1.jpg" alt="">
        </div>
        <div class="background-rightside">
              <img src="<?= base_url() ?>img/right-side/bg-3_1.jpg" alt="">
        </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content row">
                    <div class="col-md-5">
                        <?php $this->load->view('proxima'); ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SERVICES PAGE -->        
    <!-- pricing PAGE -->
      <!--<section class="cospo-page" id="Pasarelles">
          <div class="background">
              <img src="<?= base_url() ?>img/fashion-designer/bg/bg-3_1.jpg" alt="">
          </div>
          <div class="background-rightside">
              <img src="<?= base_url() ?>img/right-side/bg_4.jpg" alt="">
          </div>
        <div class="wrapper-scroll">
          <div class="scroll">
            <div class="content row">
              <div class="col-md-5">
                <h2>Accesos</h2>
                <p>
                  Enjoy intuitive features of our digital devices. Portability, connectivity, user interface, storage, performance, security and applications capability – all these makes daily life much easier and more effective.
                </p>
                <div class="pricing">
                  <div class="pricing-name">
                    <p>
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" fill="#ffffff">
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M32,8c0,3.3-2.7,6-6,6c-0.2,0-0.5,0-0.7-0.1c-1.3,3.9-4.5,7.5-8.3,8V28h3
                      	c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1h3v-6.1c-3.8-0.5-6.9-4.1-8.3-8C6.5,14,6.3,14,6,14
                      	c-3.3,0-6-2.7-6-6c0-4-0.8-4,6-4c0-2.2,1.8-4,4-4h12c2.2,0,4,1.8,4,4C32.8,4,32,4,32,8z M6,6c-4,0-4-0.2-4,2c0,2.2,1.8,4,4,4V6z
                      	 M24,4c0-1-1-2-2-2H10c-1,0-2,1-2,2v6c0,4.4,3.6,10,8,10c4.4,0,8-5.6,8-10V4z M26,6v6c2.2,0,4-1.8,4-4C30,5.8,29.9,6,26,6z"/>
                      </svg>
                      Pasarel.la 1
                    </p>
                  </div>
                  <div class="pricing-price">
                    <p>
                      18h
                      <span>sala sant Pau</span>
                    </p>
                  </div>
                  <div class="pricing-buy">
                    <button type="button" name="button">Info</button>
                  </div>
                </div>
                <div class="pricing">
                  <div class="pricing-name">
                    <p>
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 32 24" enable-background="new 0 0 32 24" xml:space="preserve" fill="#ffffff" width="30">
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M22.5,24c-0.2,0-0.3,0-0.5,0v0H8v0c-4.5-0.2-8-3.7-8-8c0-3.6,2.5-6.7,6-7.7
                      	C6.4,3.7,10.3,0,15,0c3.5,0,6.6,2.1,8.1,5c5,0.3,8.9,4.4,8.9,9.5C32,19.7,27.7,24,22.5,24z M21.5,7c-0.8-2.9-3.1-5-6.5-5
                      	c-4.3,0-6.9,3.4-6.9,7.5c0,0.1,0,0.2,0,0.4c-3.5,0.4-6.2,2.4-6.2,5.7c0,3.6,2.9,6.4,7,6.4v0h12.3v0c0.1,0,0.2,0,0.4,0
                      	c4.7,0,8.5-3.6,8.5-8C30,9.6,26.2,7,21.5,7z"/>
                      </svg>
                       Pasarel.la 2
                    </p>
                  </div>
                  <div class="pricing-price">
                    <p>
                      12h
                      <span>sala sant Pau</span>
                    </p>
                  </div>
                  <div class="pricing-buy">
                    <button type="button" name="button">INFO</button>
                  </div>
                </div>
                <div class="pricing">
                  <div class="pricing-name">
                    <p>
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 32 30" enable-background="new 0 0 32 30" xml:space="preserve" fill="#ffffff">
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M28,30H4c-2.2,0-4-1.8-4-4V8c0-2.2,1.8-4,4-4h6V2c0-1.1,0.9-2,2-2h8
                      	c1.1,0,2,0.9,2,2v2h6c2.2,0,4,1.8,4,4v18C32,28.2,30.2,30,28,30z M20,3c0-0.6-0.4-1-1-1h-6c-0.6,0-1,0.4-1,1v1c0.6,0,0.4,0,1,0h6
                      	c0.6,0,0.4,0,1,0V3z M30,8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v5h28V8z M14,16c0,1.1,0.9,2,2,2s2-0.9,2-2c0-0.4-0.1-0.7-0.3-1h-3.4
                      	C14.1,15.3,14,15.6,14,16z M30,15H19.9c0.1,0.3,0.1,0.7,0.1,1c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-0.3,0.1-0.7,0.1-1H2v11c0,1.1,0.9,2,2,2
                      	h24c1.1,0,2-0.9,2-2V15z"/>
                      </svg>
                      Pasarel.la 2
                    </p>
                  </div>
                  <div class="pricing-price">
                    <p>
                      22H
                      <span>sala sant Pau</span>
                    </p>
                  </div>
                  <div class="pricing-buy">
                    <button type="button" name="button">INFO</button>
                  </div>
                </div>
                <div class="pricing">
                  <div class="pricing-name">
                    <p>
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 31 29" style="enable-background:new 0 0 31 29;" xml:space="preserve" fill="#ffffff">
                        <path
                          style="fill-rule:evenodd;clip-rule:evenodd;"
                          d="M30.113,16.487l-13.152,5.9c-1.259,0.547-1.761,0.547-2.923,0l-13.152-5.9
                      c-0.698-0.369-0.887-1.739-0.887-2.486c0.879,0.501,2.113,1.058,2.067,1.03l13.433,6.18l13.433-6.18
                      c0.023-0.005,1.221-0.608,2.067-1.03C31,14.766,30.762,16.186,30.113,16.487z M30.113,10.308l-13.152,5.9
                      c-1.259,0.547-1.761,0.547-2.923,0l-13.152-5.9c-1.033-0.547-1.001-2.27,0-2.913l13.152-6.93c1.162-0.579,1.89-0.611,2.923,0
                      l13.152,6.93C31.114,7.942,31.082,9.858,30.113,10.308z M15.499,1.641L2.066,8.851L15.499,14.5l13.433-5.649L15.499,1.641z
                       M15.499,1.641 M15.499,27.391l13.433-6.18c0.023-0.005,1.221-0.608,2.067-1.03c0,0.765-0.238,2.185-0.887,2.486l-13.152,5.899
                      c-1.259,0.547-1.761,0.547-2.923,0L0.886,22.667c-0.698-0.37-0.887-1.739-0.887-2.486c0.879,0.501,2.113,1.058,2.067,1.03
                      L15.499,27.391z"/>
                      </svg>
                      Pasarel.la VIP
                    </p>
                  </div>
                  <div class="pricing-price">
                    <p>
                      19h
                      <span>sala sant Pau</span>
                    </p>
                  </div>
                  <div class="pricing-buy">
                    <button type="button" name="button">INFO</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END pricing PAGE --> 
     
    <!-- FAQ PAGE -->
    <section class="cospo-page" id="faq">
        <div class="background">
            <img src="<?= base_url() ?>img/law-firm/bg/bg-5.jpg" alt=""/>
        </div>
        <div class="background-rightside">
              <img src="<?= base_url() ?>img/right-side/bg_6.jpg" alt="">
          </div>
        <div class="wrapper-scroll">
            <div class="scroll" style="background-color: rgba(0, 0, 0, 0.0);">
                <div class="content col-md-5">
                    <?php $this->load->view('language/'.$_SESSION['lang'].'/faq'); ?>                        
                </div>
            </div>
        </div>
    </section>
    <!-- END FAQ PAGE -->
    <!-- CONTACT PAGE -->
    <section class="cospo-page" id="contacte">
        <div class="background">
            <img src="<?= base_url() ?>img/fashion-designer/bg/bg-5_3.jpg" alt="">
        </div>
        <div class="background-rightside">
              <img src="<?= base_url() ?>img/right-side/bg_7.jpg" alt="">
          </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content row">
                    <div class="col-md-5">
                      <h2>Contacte</h2>
                        <p>ENTITAT ORGANITZADORA:
                            <br/>
                            <br>
                            <img src='<?= base_url() ?>img/svg/logomodacc.svg' style="width:29%; margin-bottom: 2%">
                            c/ Milà i Fontanals, 16 – 24. 08012 (Barcelona).  
                            <button data-action="show-map" class="show-map">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 18 23" style="enable-background:new 0 0 18 23;" xml:space="preserve" fill="#ffffff">
                                <path
                                    style="fill-rule:evenodd;clip-rule:evenodd;"
                                    d="M9,0C4.029,0,0,4.183,0,9.344S9,23,9,23s9-8.496,9-13.656S13.97,0,9,0z
                                    M9,17.25c-4.206,0-7.616-3.54-7.616-7.906S4.794,1.437,9,1.437c4.206,0,7.615,3.54,7.615,7.906S13.205,17.25,9,17.25z"/>
                                <path style="fill-rule:evenodd;clip-rule:evenodd;" d="M9,7.906c0.765,0,1.385,0.644,1.385,1.438c0,0.794-0.62,1.438-1.385,1.438
                                      c-0.765,0-1.385-0.644-1.385-1.438C7.615,8.55,8.235,7.906,9,7.906z"/>
                                </svg>
                            </button>
                        </p>
                        <?php $this->load->view('contacto'); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Google map API -->
        <div class="map">
            <div class="map-canvas" id='mapacontacto' data-zoom='15' data-lat='41.4136254' data-lon='2.1744992999999795' data-icon='<?= base_url('img/svg/point.svg') ?>'></div>
        </div>
    </section>
    <!-- END CONTACT PAGE -->        
           
    
    <!-- Demana PAGE -->
    <section class="cospo-page" id="demana">
        <div class="background">
            <img src="<?= base_url() ?>img/fashion-designer/bg/bg-5_2.jpg" alt="">
        </div>
        <div class="background-rightside">
              <img src="<?= base_url() ?>img/right-side/bg_9.jpg" alt="">
          </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <div class="content row">
                    <?php $this->load->view('language/'.$_SESSION['lang'].'/solicita'); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- END Demana PAGE -->

    <!-- BLOG PAGE -->
    <section class="cospo-page blog" id="blog">
        <div class="background">
            <img src="<?= base_url() ?>img/restaurant/bg/bg-5.jpg" alt="">
        </div>
        <div class="wrapper-scroll">
            <div class="scroll">
                <?php $this->load->view('language/'.$_SESSION['lang'].'/blog'); ?>                
            </div>
        </div>
    </section>
    <!-- END BLOG PAGE -->
    
</main>
