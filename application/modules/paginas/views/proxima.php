<?php
    $this->db->select('ediciones.nombre, marcas.nombre as marca, marcas.id as marcaid, marcas.logo');
    $this->db->join('marcas','marcas.id = ediciones_marcas.marcas_id');
    $this->db->join('ediciones','ediciones.id = ediciones_marcas.ediciones_id');
    $this->db->group_by('marcas.id');
    $this->db->order_by('marcas.nombre','ASC');
    $expositors = $this->db->get_where('ediciones_marcas',array('ediciones.idioma'=>$_SESSION['lang'],'activa'=>1));
    if($expositors->num_rows()>0): 
?>
<h2><?= $expositors->row()->nombre ?></h2>
<p>
    A continuació, podeu trobar una llista de les nostres marques expositores.
</p>
<br>
<div class="col-md-6">
    <ul class="gallery row gallery-services" data-item="7">
        <?php foreach($expositors->result() as $e): ?>
            <li class="col-sm-3">
                <a href="#proxima" data-action="ajax/frontend/read/programacio/<?= $e->marcaid ?>">
                    <div style="background: url(<?= base_url('img/marcas/'.$e->logo) ?>); background-position:center; background-size:cover;" class="expositorsLogo"></div>
                    <span>
                        <?= $e->marca ?>
                    </span>
                    <p></p>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
</div>
<?php else: ?>
    <h2><?= $this->db->get_where('ediciones',array('ediciones.idioma'=>$_SESSION['lang'],'activa'=>1))->row()->nombre ?></h2>
    <p>
        A continuació, podeu trobar una llista de les nostres marques expositores.
    </p>
    <br>
    <div class="col-md-6">
        Per definir
    </div>    
<?php endif ?>