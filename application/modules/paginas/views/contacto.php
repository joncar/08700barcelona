<p>EMAIL:<a href="mailto:info@modacc.cat">  info@modacc.cat</a></p>
<p>TELÈFON:<a href="tel:info@080barcelonashowroom.com">  +(34)93 415 12 28</a> <br>
<p>HORARI: 9h a 13h i de 15h a 18h</p>
<div class="widget-item">
    <a href="https://www.linkedin.com/company/cl%C3%BAster-t%C3%A8xtil-i-moda-de-catalunya/
" class="button socicon-linkedin dark-blue"></a>
    <a href="https://twitter.com/modaccat" class="button socicon-twitter light-blue"></a>
    <a href="https://www.instagram.com/modaccat/" class="button socicon-instagram dark-red"></a>
</div>
<h4>El teu missatge</h4>
<div id="Conmessage"></div>
<form  action="#" onsubmit="return contactar()" method="post">
    <div class="row">
        <div class="col-md-3">
            <input type="text" id="nomCon" placeholder="Nom" name="name">
        </div>
        <div class="col-md-3">
            <input type="email" id="emailCon" placeholder="Email" name="email">
        </div>
    </div>
    <textarea id="messageCon" placeholder="Comentari" class="single-line" name="comment"></textarea>
    <div class="form-message"></div>
    <button type="submit">Enviar</button>
</form>