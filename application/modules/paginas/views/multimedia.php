<div class="col-md-5">
    <h2 style="margin-bottom: 0;">080 show media</h2>    
    <div class="row" style="margin:20px;">
        <?php 
            $this->db->order_by('id','DESC');
            $ediciones = $this->db->get_where('ediciones',array('ediciones.idioma'=>$_SESSION['lang'],'activa'=>0));            
            foreach($ediciones->result() as $e):
        ?>
            <!--<button class="edicion" data-id="<?= $e->id ?>"><?= $e->nombre ?></button>-->
        <?php endforeach ?>
    </div>  
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-6 padding-0">
            <?php foreach($ediciones->result() as $n=>$e): ?>
                <ul id="" class="gallery <?= $n==0 || 1==1?'':'ocultar' ?> no-padding-li-sm row gal<?= $e->id ?>" data-item="6" style="display: none">                    

                    <?php 
                        $this->db->join('ediciones_fotos','ediciones_fotos.id = multimedia.ediciones_id');                        
                        $this->db->order_by('tipo','DESC');
                        foreach($this->db->get_where('multimedia',array('multimedia.ediciones_fotos_id'=>$e->id))->result() as $a): ?>
                        <li class="col-xs-2 col-sm-2">
                            <a href="#about" data-action="ajax/frontend/multimedia/<?= $a->id ?>">
                                <img src="<?= base_url('img/multimedia/'.$a->url) ?>" alt="">
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endforeach ?>
        </div>
    </div>        
</div>
<script type="text/javascript">    
    $(document).on('ready',function(){
        setTimeout(function(){$(".ocultar").hide();},1000);
    });
    $(document).on('click','.edicion',function(){        
        $(".gallery").hide();
        $(".gal"+$(this).data('id')).show();
    });
</script>