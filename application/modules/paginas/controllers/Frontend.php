<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }        
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->seminarios();        
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array('seminarios'=>$this->seminarios),TRUE),
                    'title'=>ucfirst(str_replace('-',' ',$url))                    
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                $page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                $page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nom','Nombre','required');
            $this->form_validation->set_rules('message','Comentario','required');
            if($this->form_validation->run()){
                $this->enviarcorreo((object)$_POST,3,'info@080barcelonashowroom.cat');
                echo $this->success('Gracias por contactarnos, en breve le llamaremos');
            }else{
                echo $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
            }
            
        }
        
        function subscribir(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_POST['id'] = $this->querys->encodeid($this->db->insert_id());
                    switch($_SESSION['lang']){
                        case 'es': $this->enviarcorreo((object)$_POST,7); echo $this->success('Subscrito satisfactoriamente'); break;
                        case 'en': $this->enviarcorreo((object)$_POST,8); echo $this->success('Subscription Successfull'); break;
                        case 'ca': $this->enviarcorreo((object)$_POST,9); echo $this->success('subscripció correcta'); break;
                    }                    
                }else{
                    switch($_SESSION['lang']){
                        case 'es': echo $this->error('Correo ya existente'); break;
                        case 'en': echo $this->error('Email already exists'); break;
                        case 'ca': echo $this->error('Email ja registrat'); break;
                    }
                    
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }  
        
        function unsubscribe($id = ''){
            /*if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{*/
            $id = $this->querys->decodeid($id);
            if(is_numeric($id)){
                $emails = $this->db->get_where('subscritos',array('id'=>$id));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('id'=>$id));                    
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }
                
                $emails = $this->db->get_where('boletin_email',array('id'=>$id));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->update('boletin_email',array('status'=>0),array('id'=>$id));
                    echo $this->success('Correo desafiliado al sistema de newsletter');
                }
                
                echo $this->success('El correo ya ha sido eliminado');
               // $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }else{
                echo $this->success('El correo ya ha sido eliminado');
            }
            //}
        }
    }
                        