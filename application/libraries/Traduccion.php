<?php    
class Traduccion{
    public function traducir($view,$idioma = ''){        
        if($idioma!='ca'){
            require_once APPPATH.'language/web/'.$idioma.'.php';
            foreach($lang as $n=>$v){
                $view = str_replace($n,$v,$view);
            }
        }
        return $view;
    }
}    
